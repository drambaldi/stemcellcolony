/* http://catlikecoding.com/unity/tutorials/graphs/ */
using UnityEngine;
using System.Collections;

public class Grapher2 : MonoBehaviour {
	
	public enum FunctionOption {
		Linear,
		Exponential,
		Parabola,
		Sine,
		Ripple
	}
	
	/* We first define a delegate type for methods that have a single float as both input and output, 
	 * which corresponds to our function methods. We call it FunctionDelegate. 
	 * Then we add a static array named functionDelegates and fill it with delegates 
	 * to our methods, in the same order that we named them in our enumeration. */
	private delegate float FunctionDelegate (Vector3 p, float x);
	private static FunctionDelegate[] functionDelegates = {
		Linear,
		Exponential,
		Parabola,
		Sine,
		Ripple
	};
		
	public FunctionOption function;
	
	private ParticleEmitter emitter;
	
	private Particle[] points;
	public int resolution = 20;
	private int currentResolution;
	
	void Start () {
		emitter = GetComponent<ParticleEmitter>();
		CreatePoints();
	}
	
	void Update ()
	{
		if(currentResolution != resolution){
			CreatePoints();
		}
		
		/* Now we can select the desired delegate from the array based on our function variable, 
		 * by casting it to an integer. We store this delegate in a temporary variable and use it to calculate the value of Y. */
		FunctionDelegate f = functionDelegates[(int)function];
		
		// set the Color and Posiiton. 
		for(int i = 0; i < points.Length; i++){
			Color c = points[i].color;
			c.a = f(points[i].position, Time.timeSinceLevelLoad);
			points[i].color = c;
		}
		
		/* We need to assign the particle array back to the emitter.
		 * That will copy our data back into the particle system, replacing its old particles. */
		emitter.particles = points;
	}
	
	private void CreatePoints()
	{
		/* MAX PARTICLES = 16129
		 * if 1 caluculation of increment will result in a division by zero 
		 * This is because there is a limit to how many particles a Particle Renderer will display. 
		 * So it's a good idea to limit the resolution to 127, which translates to 16129 points. */
		
		if(resolution < 2){
			resolution = 2;
		}else if(resolution > 127){
			resolution = 127;
		}
		
		/* A simple way to detect a change of resolution is by storing it twice 
		 * and then constantly checking whether both values are still the same. 
		 * If at some point they're different, we need to rebuild the graph. */
		currentResolution = resolution;

		/* The first point should be placed at 0 and the last should be placed at 1.
		 * All other points should be placed in between. 
		 * So the distance – or X increment – between two points is 1 / (resolution - 1). */
		float increment = 1f / (resolution - 1);		

		emitter.ClearParticles();
		emitter.Emit(resolution * resolution);
		
		points = emitter.particles;
		
		int i = 0;
		for(int x = 0; x < resolution; x++){
			for(int z = 0; z < resolution; z++){
				for(int y = 0; y < resolution; y++){
					Vector3 p = new Vector3(x, y, z) * increment;
					p = p + transform.position;
					points[i].position = p;
					points[i++].color = new Color(p.x, p.y, p.z);
				}
			}
		}
	}
	
	private static float Linear (Vector3 p, float t) {
		return p.x;
	}
	
	private static float Exponential (Vector3 p, float t) {
		return p.x * p.x;
	}
	
	private static float Parabola (Vector3 p, float t){
		p.x = 2f * p.x - 1f;
		p.z = 2f * p.z - 1f;
		return 1f - p.x * p.x * p.z * p.z;
	}
	
	private static float Sine (Vector3 p, float t){
		return 0.50f +
			0.25f * Mathf.Sin(4 * Mathf.PI * p.x + 4 * t) * Mathf.Sin(2 * Mathf.PI * p.z + t) +
			0.10f * Mathf.Cos(3 * Mathf.PI * p.x + 5 * t) * Mathf.Cos(5 * Mathf.PI * p.z + 3 * t) +
			0.15f * Mathf.Sin(Mathf.PI * p.x + 0.6f * t);
	}
	
	private static float Ripple (Vector3 p, float t){
		float squareRadius = (p.x - 0.5f) * (p.x - 0.5f) + (p.z - 0.5f) * (p.z - 0.5f);
		return 0.5f + Mathf.Sin(15 * Mathf.PI * squareRadius - 2f * t) / (2f + 100f * squareRadius);
	}
}
