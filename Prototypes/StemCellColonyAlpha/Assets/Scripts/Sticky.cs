using UnityEngine;
using System.Collections;

public class Sticky : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	// CollisionEnter
	void OnCollisionEnter(Collision c) {
    	var joint = gameObject.AddComponent<FixedJoint>();
    	joint.connectedBody = c.rigidbody;
    }
}
