using UnityEngine;
using System.Collections;

public class TestMouseMove : MonoBehaviour {
	
	float speed = 5.0f;
	
	public float sensitivityX = 8F;
    public float sensitivityY = 8F;
	private float scroll;
	
    float mHdg = 0F;
    float mPitch = 0F;
	
	// Use this for initialization
	void Start () {
	
	}
	
	void OnGUI()
	{
	}
	
	// Update is called once per frame
	void Update () 
	{
		//GET MOUSE SCROLL WHEEL AXIS VALUE (RANGE -0.5 TO 0.5)
    	scroll = Input.GetAxis("Mouse ScrollWheel"); 
    	if (scroll != 0) 
			Camera.main.fieldOfView += scroll;
		
		
		if (!(Input.GetMouseButton(0) || Input.GetMouseButton(1)))
            return;

        float deltaX = Input.GetAxis("Mouse X") * sensitivityX;
        float deltaY = Input.GetAxis("Mouse Y") * sensitivityY;

        if (Input.GetMouseButton(0) && Input.GetMouseButton(1))
        {
            Strafe(deltaX);
            ChangeHeight(deltaY);
        }
        else
        {
            if (Input.GetMouseButton(0))
            {
                MoveForwards(deltaY);
                ChangeHeading(deltaX);
            }
            else if (Input.GetMouseButton(1))
            {
                ChangeHeading(deltaX);
                ChangePitch(-deltaY);
            }
        }
		
		float x = Input.GetAxis("Horizontal") * Time.deltaTime * speed;
		float z = Input.GetAxis("Vertical") * Time.deltaTime * speed;
		transform.Translate(x, 0, z);
	}
	
	void MoveForwards(float aVal)
    {
        Vector3 fwd = transform.forward;
        fwd.y = 0;
        fwd.Normalize();
        transform.position += aVal * fwd;
    }

    void Strafe(float aVal)
    {
        transform.position += aVal * transform.right;
    }

    void ChangeHeight(float aVal)
    {
        transform.position += aVal * Vector3.up;
    }

    void ChangeHeading(float aVal)
    {
        mHdg += aVal;
        WrapAngle(ref mHdg);
        transform.localEulerAngles = new Vector3(mPitch, mHdg, 0);
    }

    void ChangePitch(float aVal)
    {
        mPitch += aVal;
        WrapAngle(ref mPitch);
        transform.localEulerAngles = new Vector3(mPitch, mHdg, 0);
    }

    public static void WrapAngle(ref float angle)
    {
        if (angle < -360F)
            angle += 360F;
        if (angle > 360F)
            angle -= 360F;
    }
}
