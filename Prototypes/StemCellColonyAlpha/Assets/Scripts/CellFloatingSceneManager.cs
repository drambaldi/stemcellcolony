using UnityEngine;
using System.Collections;

public class CellFloatingSceneManager : MonoBehaviour {
	
	public static CellFloatingSceneManager SM;
	public GameObject cellPrefab;
	public GameObject cellOrigin;
	
	private GameObject lastCell;	
	private ArrayList cellList;
	private int celldivisions;
	
	// Use this for initialization
	void Awake() 
	{
		SM = this;
		celldivisions = 0;
		cellList = new ArrayList();
		cellList.Add(cellOrigin);
		lastCell = cellOrigin;
		celldivisions++;						
	}
		
	// Update is called once per frame
	void Update () 
	{
		if(Input.GetButtonDown("Jump"))
		{
			DuplicateCell();
		}
	}
	
	void OnGUI()
	{
		GUILayout.Label("CELL PROTOTYPES");
		GUILayout.Label("PRESS SPACE FOR DUPLICATE LAST CELL");
		GUILayout.Label("CELL DIVISIONS = " + celldivisions);
		GUILayout.Label("CURRENT CELLS = " + cellList.Count);
	}
	
	void DuplicateCell()
	{
		Vector3 lastCellPos =  lastCell.transform.position;
		Destroy(lastCell);
		Vector3 newCellPosOne = lastCellPos + new Vector3(0.5f,0.0f,0.0f);
		Vector3 newCellPosTwo = lastCellPos + new Vector3(-0.5f,0.0f,0.0f);
			
		GameObject empty = Instantiate(new GameObject("doppietto"),lastCellPos, Quaternion.identity) as GameObject;		
		GameObject cellOne = Instantiate(cellPrefab, newCellPosOne, Quaternion.identity) as GameObject;
		GameObject cellTwo = Instantiate(cellPrefab, newCellPosTwo, Quaternion.identity) as GameObject;
			
		cellOne.transform.parent = empty.transform;
		cellTwo.transform.parent = empty.transform;
			
		Vector3 randRotation = new Vector3(Random.value,Random.value,Random.value);
    	empty.transform.Rotate(randRotation * Random.Range(0.0f, 360.0f));
			
		cellOne.transform.parent = null;
		cellTwo.transform.parent = null;
			
		Destroy(empty);
		
		cellList.Add(cellOne);
		cellList.Add(cellTwo);
		
		celldivisions++;
	
		if (celldivisions % 2 == 0) 
		{	
			lastCell = cellOne;
		} else 
		{
			lastCell = cellTwo;
		}
	}
	
	//little generic helper function that will select a random enum member.
	static T GetRandomEnum<T>()
	{
    	System.Array A = System.Enum.GetValues(typeof(T));
    	T V = (T)A.GetValue(UnityEngine.Random.Range(0,A.Length));
    	return V;
	}
}
