using UnityEngine;
using System.Collections;

public class TestRotation : MonoBehaviour {
	
	public GameObject cellOrigin;
	public GameObject cellPrefab;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetButtonDown("Jump"))
		{
			Vector3 lastCellPos =  cellOrigin.transform.position;
			Destroy(cellOrigin);
			Vector3 newCellPosOne = lastCellPos + new Vector3(0.5f,0.0f,0.0f);
			Vector3 newCellPosTwo = lastCellPos + new Vector3(-0.5f,0.0f,0.0f);
			
			GameObject empty = Instantiate(new GameObject("doppietto"),lastCellPos, Quaternion.identity) as GameObject;
			
			GameObject cellOne = Instantiate(cellPrefab, newCellPosOne, Quaternion.identity) as GameObject;
			GameObject cellTwo = Instantiate(cellPrefab, newCellPosTwo, Quaternion.identity) as GameObject;
			
			cellOne.transform.parent = empty.transform;
			cellTwo.transform.parent = empty.transform;
			
			Vector3 randRotation = new Vector3(Random.value,Random.value,Random.value);
    		empty.transform.Rotate(randRotation * Random.Range(0.0f, 360.0f));
			
			cellOne.transform.parent = null;
			cellTwo.transform.parent = null;
			
			Destroy(empty);
		}
	}
}
