using UnityEngine;
using System.Collections;
using System;
using System.Text;
using System.IO;

public class SampleRandom : MonoBehaviour {
	
	private MersenneTwister mrand;
	private int linecounter;
	private int maxsampling;
	private String path;
	private String filename;
	private FileStream fs;
	private ArrayList randomList;
	
	// Use this for initialization
	void Start () {
		mrand = new MersenneTwister();
		linecounter = 0;
		maxsampling = 100;
		filename = "/sampling.txt";
		path = Application.dataPath + filename;
		fs = File.Create(path);
		randomList = new ArrayList();
	}
	
	// Update is called once per frame
	void Update () {
		
		if (linecounter < maxsampling) {
			double rn = mrand.NextDouble();
			String data =  rn + "\r";
			AddText(fs, data);
			randomList.Add(rn);
			linecounter++;
		} else if (linecounter == maxsampling) {
			randomList.Sort();
			foreach(object obj in randomList) Debug.Log(obj);
		}
	}
	
	private static void AddText(FileStream fs, string value)
    {
        byte[] info = new UTF8Encoding(true).GetBytes(value);
        fs.Write(info, 0, info.Length);
    }
	
	void OnGUI()
	{
		if (linecounter == maxsampling) {
			GUILayout.Label("SAMPLING COMPLETE");
		} else {
			GUILayout.Label("SAMPLING: " + linecounter);
		}
	}
}
