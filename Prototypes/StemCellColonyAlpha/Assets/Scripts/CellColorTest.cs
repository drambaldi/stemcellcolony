using UnityEngine;
using System.Collections;

public class CellColorTest : MonoBehaviour {
	
	public GameObject cell;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetButtonDown("Jump"))
		{
			cell.transform.renderer.material.color = new Color(0.2F, 0.3F, 0.4F, 0.5F);
		}
		
		if (Input.GetKey("up"))
		{
			Color current_color = cell.transform.renderer.material.color;
			current_color.r += 0.01f;
			cell.transform.renderer.material.color = current_color;
			Debug.Log(current_color.r);
		}
        
        
        if (Input.GetKey("down"))
		{
			Color current_color = cell.transform.renderer.material.color;
			current_color.r -= 0.01f;
			cell.transform.renderer.material.color = current_color;
			Debug.Log(current_color.r);
		}
	}
}
