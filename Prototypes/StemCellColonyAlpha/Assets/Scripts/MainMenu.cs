using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour {

	void OnGUI()
	{
		GUILayout.BeginArea(new Rect(0, 0, Screen.width, Screen.height));
		GUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace();
		if (GUILayout.Button("Cell Physics One"))
		{
			Application.LoadLevel("CellPrototype");
		}
		
		if (GUILayout.Button("BuoyantObject"))
		{
			Application.LoadLevel("BuoyantObject");
		}
		
		if (GUILayout.Button("JointCells"))
		{
			Application.LoadLevel("JointCells");
		}
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		GUILayout.EndArea();
	}
}
