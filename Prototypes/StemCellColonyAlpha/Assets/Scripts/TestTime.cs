using UnityEngine;
using System.Collections;

public class TestTime : MonoBehaviour {
	
	public bool pause;
	public int speed; 
	
	public GameObject cellPrefab;
	
	private float lastCellTime;
	private float [] speedValues =	new float [] { 0.0f, 0.001f, 0.01f, 0.1f, 1.0f, 10.0f };
	
	// Use this for initialization
	void Start () {
		pause = false;
		speed = 4;
		lastCellTime = 0;
	}
	
	// Update is called once per frame
	void Update () {
		
		if (pause) {
			Time.timeScale = 0.0f;
		} else {
			Time.timeScale = speedValues[speed];
			// The fixed delta time will now be 0.02 frames per real-time second
			Time.fixedDeltaTime = 0.02f * Time.timeScale;
			
			eachSecondCell();			
		}
		
		if (Input.GetButtonDown("Jump")) {
			pause = !pause;
		}
		
	}
	
	void OnGUI()
	{
		GUILayout.Label("CURRENT TIME = " + Time.time);
		GUILayout.Label("CURRENT SPEED = " + Time.timeScale);
		GUILayout.Label("SPACE FOR PAUSE/RESUME.");
		GUILayout.Label("SPEED:" + speed);
		speed = (int) GUILayout.HorizontalSlider(speed, 0.0f, 5.0f);
	}
	
	void eachSecondCell()
	{
		//Make a Prefab each unit time.
		if ( Mathf.Floor(Time.time) % 1 == 0 && Time.time > (lastCellTime + 1.0f)) {
			Debug.Log("TIME");
			if (!(cellPrefab)) {
				Debug.LogWarning("You forgot to set the cell prefab!");
				return;
			}
			Instantiate(cellPrefab, randomVect3(), Quaternion.identity);
			lastCellTime = Time.time;
		}
	}
	
	Vector3 randomVect3()
	{
		float range = 5.0f;
		Vector3 vect = new Vector3(Random.Range(-range,range),Random.Range(-range,range),Random.Range(-range,range));
		return vect;
	}
}
