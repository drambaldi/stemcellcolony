using UnityEngine;
using System.Collections;

public class Grapher1 : MonoBehaviour {
	
	public enum FunctionOption {
		Linear,
		Exponential,
		Parabola,
		Sine
	}
	
	/* We first define a delegate type for methods that have a single float as both input and output, 
	 * which corresponds to our function methods. We call it FunctionDelegate. 
	 * Then we add a static array named functionDelegates and fill it with delegates 
	 * to our methods, in the same order that we named them in our enumeration. */
	private delegate float FunctionDelegate (float x);
	private static FunctionDelegate[] functionDelegates = {
		Linear,
		Exponential,
		Parabola,
		Sine
	};
		
	public FunctionOption function;
	
	private ParticleEmitter emitter;
	
	private Particle[] points;
	public int resolution = 10;
	private int currentResolution;
	
	void Start () {
		emitter = GetComponent<ParticleEmitter>();
		CreatePoints();
	}
	
	void Update ()
	{
		if(currentResolution != resolution){
			CreatePoints();
		}
		
		/* Now we can select the desired delegate from the array based on our function variable, 
		 * by casting it to an integer. We store this delegate in a temporary variable and use it to calculate the value of Y. */
		FunctionDelegate f = functionDelegates[(int)function];
		
		// set the Y position of the points. 
		for(int i = 0; i < resolution; i++){
			Vector3 p = points[i].position;
			p.y = f(p.x);
			points[i].position = p;
			Color c = points[i].color;
			c.g = p.y;
			points[i].color = c;
		}
		
		/* We need to assign the particle array back to the emitter.
		 * That will copy our data back into the particle system, replacing its old particles. */
		emitter.particles = points;
	}
	
	private void CreatePoints()
	{
		// if 1 caluculation of increment will result in a division by zero
		if(resolution < 2){
			resolution = 2;
		}
		
		/* A simple way to detect a change of resolution is by storing it twice 
		 * and then constantly checking whether both values are still the same. 
		 * If at some point they're different, we need to rebuild the graph. */
		currentResolution = resolution;

		/* The first point should be placed at 0 and the last should be placed at 1.
		 * All other points should be placed in between. 
		 * So the distance – or X increment – between two points is 1 / (resolution - 1). */
		float increment = 1f / (resolution - 1);		

		emitter.ClearParticles();
		emitter.Emit(resolution);
		
		points = emitter.particles;
		
		for(int i = 0; i < resolution; i++){
			float x = i * increment;
			points[i].position = new Vector3(x, 0f, 0f);
			points[i].color = new Color(x, 0f, 0f);
		}
	}
	
	private static float Linear (float x) {
		return x;
	}
	
	private static float Exponential (float x) {
		return x * x;
	}
	
	private static float Parabola (float x){
		x = 2f * x - 1f;
		return x * x;
	}
	
	private static float Sine (float x){
		return 0.5f + 0.5f * Mathf.Sin(2 * Mathf.PI * x + Time.timeSinceLevelLoad);
	}
}
