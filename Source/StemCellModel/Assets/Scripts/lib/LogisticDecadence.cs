using System;
using UnityEngine;

public class LogisticDecadence {

	public static float LogDecadence(float temp, int size, int maxsize)
	{
		return( 2/(1+Mathf.Exp(temp*( size - maxsize ))) );
	}
}
