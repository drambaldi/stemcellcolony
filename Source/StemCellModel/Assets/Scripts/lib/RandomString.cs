using System;
using System.Text;

public class RandomString {
	public static string MakeRandomString (int size)
	{
		StringBuilder builder = new StringBuilder();
		MersenneTwister mrand = new MersenneTwister();
		char ch ;
		for(int i=0; i<size; i++)
		{
			// see ASCII table 97-122 is [a-z]
			ch = Convert.ToChar(mrand.NextUInt32(97,122));
			builder.Append(ch);
		}
		return builder.ToString();
	}
}
