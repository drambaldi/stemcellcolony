/*
 * SIMULATION MANAGER
 * 
 * */

using UnityEngine;
using System.Collections;

public class SimulationManager : MonoBehaviour {
	
	// ORBIT POINT
	public Transform orbitPoint;
	
	/* 
	 * 
	 * TIME: 
	 * - oneDayConstant: How many seconds form one day?
	 * 	- Can't be changed after Awake
	 * 
	 * - time is Time.timeSinceLevelLoad
	 * - seconds store time * secondMultiplier (teh seconds in simulation time)
	 * 
	 *    :::::::   secondMultiplier = DAY / oneDayConstant;   :::::::::
	 * 	 where DAY is in SECONDs
	 * 
	 * - currentTimeLimit is oneDayConstant * timeLimit
	 *    
	 */
	public bool pause = true;
	public float time;
	public float seconds;
	public float [] speedValues = new float [] { 0.1f, 1.0f, 2.0f, 5.0f, 10.0f, 20.0f, 50.0f };
	public int speed = 1;
	
	public float animationSpeed = 0.1f;
	private float animationSpeedConstant = 0.1f;
	
	// show type of cells switch
	public bool showStem = true;
	private bool prevShowStem = true;
	public bool showProgenitor = true;
	private bool prevShowProgenitor = true;
	public bool showDifferentiated = true;
	private bool prevShowDifferentiated = true;
	
	public bool showStain = false;
	
	public enum CellColor
	{
		TYPE = 0,
		STAIN = 1
	}
	private CellColor previousStatusColor = CellColor.TYPE;
	
	private const float SECOND = 1;
	private const float MINUTE = 60  * SECOND;
	private const float HOUR = 60 * MINUTE;
	private const float DAY = 24 * HOUR;
	private float secondMultiplier;
	
	/* *******************************************************************************************************************************
	 * 
	 * ONEDAYCONTSANT: 
	 *  Is a key constat that define ONE DAY in game time seconds
	 * 
	 ******************************************************************************************************************************* */
	
	public const float ONEDAYCONSTAT = 10.0f; // How many seconds form one day?	
	public int timeLimit = 10;                // IS THE USER DEFINED END OF SIMULATION IN SIMULATION DAYS
	public const int ENDOFSIMULATION = 1000;  // In DAYS ...
	public float currentTimeLimit;            // IS THE CONVERTED VALUE
	private bool endOfSimulation;
	public bool simulationStarted;
	
	// RANDOM SEED
	private MersenneTwister mrand;
	public int seed = 12345;
	
	// THE GUI OBJECT
	private StemCellModelGUI theGUIObject;
	
	// THE PLAYER
	private Player player;
	
	// THE GENEALOGY MANAGER
	private GenealogyManager gmanager;
	
	// PREFAB HALO SELECTION EMPTY OBJECT
	private static string haloSelectionPrefabPath = "HaloSelectionPrefab";
	private UnityEngine.Object haloSelectionPrefab;
	private GameObject haloSelection;
	
	/* *******************************************************************************************************************************
	 * INIT 
	 * 	AWAKE --> START
	 * 
	 * Start is only called once in the lifetime of the behaviour. 
	 * The difference between Awake and Start is that Start is only called if the script instance is enabled. 
	 * This allows you to delay any initialization code, until it is really needed. Awake is always called before any Start functions. 
	 * This allows you to order initialization of scripts
	 * 
	 * ******************************************************************************************************************************* */
	
	void Awake () 
	{
		theGUIObject = GameObject.Find("GUIObject").GetComponent<StemCellModelGUI>();
		player = GameObject.Find("Player").GetComponent<Player>();
		gmanager = GameObject.Find("GenealogyManager").GetComponent<GenealogyManager>();
		mrand = new MersenneTwister(seed);
		secondMultiplier = DAY / ONEDAYCONSTAT;
		time = 0.0f;
		seconds = 0.0f;
		// LOAD THE HALO PREFAB
		haloSelectionPrefab = Resources.Load(haloSelectionPrefabPath);
		endOfSimulation = false;
		simulationStarted = false;
	}
	
	void Start()
	{
		// INSTANTIATE THE HALO PREFAB
		InstantiateHaloSelection(transform.position, Quaternion.identity);
	}
	
	// Update is called once per frame
	void Update () 
	{
		// SET CURRENT TIMELIMIT
		currentTimeLimit = ONEDAYCONSTAT * timeLimit;
		
		time = Time.timeSinceLevelLoad;
		
		// SIMULATION STARTED
		simulationStarted = time > 0 ? true : false;
			
		
		// USER DEFINED timeLimit
		if (time > currentTimeLimit && !endOfSimulation) {
			pause = true;
			endOfSimulation = true;
		}
		
		// SIMULATION END
		if (time > ENDOFSIMULATION)
			pause = true;
		
		seconds = time * secondMultiplier;
		
		if (pause) {
			Time.timeScale = 0.0f;
		} else {
			Time.timeScale = speedValues[speed];
			animationSpeed = animationSpeedConstant * speedValues[speed];
			// The fixed delta time will now be 0.02 frames per real-time second
			Time.fixedDeltaTime = 0.04f * Time.timeScale;
		}
		
		// move the Halo
		if (ActiveSelection != null) {
			// put the halo in position
			haloSelection.transform.position = ActiveSelection.transform.position;
			haloSelection.GetComponent<Light>().enabled = true;
		}

		if (showStain && previousStatusColor == CellColor.TYPE) {
			ShowStains();
			previousStatusColor = CellColor.STAIN;
		} else if (!showStain && previousStatusColor == CellColor.STAIN) {
			HideStains();
			previousStatusColor = CellColor.TYPE;
		}
		
		if (showStem && !prevShowStem) {
			ShowStems();
			prevShowStem = true;
		} else if (!showStem && prevShowStem) {
			HideStems();
			prevShowStem = false;
		}
		
		if (showDifferentiated && !prevShowDifferentiated) {
			ShowDifferentiateds();
			prevShowDifferentiated = true;
		} else if (!showDifferentiated && prevShowDifferentiated) {
			HideDifferentiateds();
			prevShowDifferentiated = false;
		}
		
		if (showProgenitor && !prevShowProgenitor) {
			ShowProgenitors();
			prevShowProgenitor = true;
		} else if (!showProgenitor && prevShowProgenitor) {
			HideProgenitors();
			prevShowProgenitor = false;
		}
		
	}
	
	/* TIME */
	
	public uint Seconds()
	{
		return ( (uint) Mathf.Floor(seconds % MINUTE) );
	}
	
	public uint Minutes()
	{
		return ( (uint) Mathf.Floor( (seconds / MINUTE) % 60 ) );
	}
	
	public uint Hours()
	{
		return ( (uint) Mathf.Floor( (seconds / HOUR) % 24 ) );
	}
	
	public uint Days()
	{
		return ( (uint) Mathf.Floor(seconds / DAY) );
	}
	
	/* 
	 * SELECTION HANDLER
	 * 
	 * - Halo Actions
	 * 
	 * */
	public void SelectWithMouse()
	{
		if (ActiveSelection) {
			Deselect(ActiveSelection);
		}
		
		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
		RaycastHit hit;
		if (Physics.Raycast(ray, out hit)) {
			switch (hit.transform.tag) {
				case "Cell":
					Select(hit.transform.gameObject);
					if (theGUIObject != null && !theGUIObject.drawInspectorPanel)
						theGUIObject.drawInspectorPanel = true;
				break;
				case "GenealogyCell":
					Select(hit.transform.gameObject);
					if (theGUIObject != null && !theGUIObject.drawGenealogyInspector)
						theGUIObject.drawGenealogyInspector = true;
				break;
				case "Doppietto":
					//Debug.Log("Doppietto");
				break;
			}	
		}
	}
	
	private void InstantiateHaloSelection(Vector3 p, Quaternion r)
	{
		haloSelection = Instantiate(haloSelectionPrefab, p, r) as GameObject;
		
		// DISABLE THE LIGHT with the HALO
		haloSelection.GetComponent<Light>().enabled = false;
	}
	
	private void ActivateHalo(GameObject _gameObject)
	{
		// put the halo in position
		haloSelection.transform.position = _gameObject.transform.position;
		haloSelection.GetComponent<Light>().enabled = true;
	}
	
	private void DisableHalo()
	{
		// put the halo back at simulation Instance position
		haloSelection.transform.position = transform.position;
		haloSelection.GetComponent<Light>().enabled = false;
	}
	
	private GameObject s_ActiveSelection;
	public  GameObject ActiveSelection
    {
    	get
    	{
    		return s_ActiveSelection;
    	}
    	set
    	{
    		s_ActiveSelection = value;
    	}
    }
	
	public void Select (GameObject _gameObject)
    {
    	ActiveSelection = _gameObject;
		ActivateHalo(_gameObject);
		MoveOrbitPoint(_gameObject.transform.position);
		_gameObject.GetComponent<Cell>().SelectCell();
    }

    public void Deselect (GameObject _gameObject)
    {
    	if (ActiveSelection == _gameObject)
    	{
			_gameObject.GetComponent<Cell>().DeselectCell();
    		ActiveSelection = null;
			DisableHalo();			
    	}
    }

    public bool IsSelected (GameObject _gameObject)
    {
    	return ActiveSelection == _gameObject;
    }
	
	/* 
	 * 
	 * EDITOR STUFFS :
	 * - DRAW GIZMO CUBE of 1
	 * 
	 * */	
	void OnDrawGizmos() 
	{
        Gizmos.color = Color.green;
        Gizmos.DrawWireCube(transform.position, new Vector3(1, 1, 1));
    }
	
	// RANDOM STUFFS
	public double GetMersenneRandDouble()
	{
		return(mrand.NextDouble());
	}
	
	public float NormalizedRandom(float temperature)
	{
		// Trasformiamo questo numero in una distribuzione normale (CDF inversa, nota anche come Gaussiana, vedi http://en.wikipedia.org/wiki/Normal_distribution)
		// facendo l'inversa della cumulative density function su una distribuzione uniforme [0,1] otteniamo una distribuzione normale (-inf,+inf) centrata a zero
		// e con deviazione standard 1
		double myval = UnityNormalDistribution.StdnormalINV(GetMersenneRandDouble());
		
		// A questo punto passiamo attraverso una funzione sigmoide (o curva logistica) con Lateratura 5 
		// Questo ci riporta a un campo (0,1), il valore 5 fa si che circa il 90% dei valori restituiti sia tra 0.45 e 0.55, aumentandolo
		// la distribuzione si stringe (piu' valori vicini a 0.5), abbassandolo si allarga, ma a differenza di una gaussiana in ogni caso
		// il valore restituito e' tra zero e uno.
		myval = myval / temperature;
		myval = 1.0 / (1.0 + Mathf.Exp((float) -myval));
		return((float) myval);
	}
	
	public void MoveOrbitPoint(Vector3 pos)
	{
		orbitPoint.transform.position = pos;
	}
	
	// PLAYER
	public void PlayerLookAt(GameObject obj)
	{
		player.StartLookAt(obj.transform.position);
	}
	
	public void PlayerMoveToGenealogyView()
	{
		if (ActiveSelection) {
			ActivateGenealogyView();
			gmanager.InitGenealogyView(ActiveSelection);
			player.MoveToGenealogy();
		}
	}
	
	public void PlayerMoveToColonyView()
	{
		DeactivateGenealogyView();
		gmanager.ClearGenealogyView();
		player.MoveToColony();
	}
	
	public Player.CameraPosition playerPosition()
	{
		return player.currentCameraPosition;
	}
	
	private void DeactivateGenealogyView()
	{
		// SHOW MAIN INTERFACE, hide GENEALOGY INTERFACE, play
		pause = false;
		theGUIObject.drawMain = true;
		theGUIObject.drawGenealogy = false;
	}
	
	private void ActivateGenealogyView()
	{
		// HIDE MAIN interface, pause, show GENEALOGY INTERFACE
		pause = true;
		theGUIObject.drawMain = false;
		theGUIObject.drawGenealogy = true;
	}
	
	// SHOW TYPES
	public void ShowStems()
	{
		GameObject[] cells = GameObject.FindGameObjectsWithTag("Cell");
		for (int i=0;i<cells.Length;i++)
		{	
			if (cells[i].GetComponent<Cell>().IsStem()) {
				cells[i].GetComponent<Cell>().MakeSolid();
			}
		}
	}
	
	public void HideStems()
	{
		GameObject[] cells = GameObject.FindGameObjectsWithTag("Cell");
		for (int i=0;i<cells.Length;i++)
		{	
			if (cells[i].GetComponent<Cell>().IsStem()) {
				cells[i].GetComponent<Cell>().MakeTransparent();
			}
		}
	}
	
	public void ShowProgenitors()
	{
		GameObject[] cells = GameObject.FindGameObjectsWithTag("Cell");
		for (int i=0;i<cells.Length;i++)
		{	
			if (cells[i].GetComponent<Cell>().IsProgenitor()) {
				cells[i].GetComponent<Cell>().MakeSolid();
			}				
		}
	}
	
	public void HideProgenitors()
	{
		GameObject[] cells = GameObject.FindGameObjectsWithTag("Cell");
		for (int i=0;i<cells.Length;i++)
		{	
			if (cells[i].GetComponent<Cell>().IsProgenitor()) {
				cells[i].GetComponent<Cell>().MakeTransparent();
			}				
		}
	}
	
	public void ShowDifferentiateds()
	{
		GameObject[] cells = GameObject.FindGameObjectsWithTag("Cell");
		for (int i=0;i<cells.Length;i++)
		{	
			if (cells[i].GetComponent<Cell>().IsDifferentiated()) {
				cells[i].GetComponent<Cell>().MakeSolid();
			}				
		}
	}
	
	public void HideDifferentiateds()
	{
		GameObject[] cells = GameObject.FindGameObjectsWithTag("Cell");
		for (int i=0;i<cells.Length;i++)
		{	
			if (cells[i].GetComponent<Cell>().IsDifferentiated()) {
				cells[i].GetComponent<Cell>().MakeTransparent();
			}				
		}
	}
	
	public void ShowStains()
	{
		GameObject[] cells = GameObject.FindGameObjectsWithTag("Cell");
		for (int i=0;i<cells.Length;i++)
		{	
			cells[i].GetComponent<Cell>().ShowStain();
		}
	}
	
	public void HideStains()
	{
		GameObject[] cells = GameObject.FindGameObjectsWithTag("Cell");
		for (int i=0;i<cells.Length;i++)
		{	
			cells[i].GetComponent<Cell>().MakeSolid();
		}
	}
}
