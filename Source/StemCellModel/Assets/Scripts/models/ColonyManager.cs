using UnityEngine;
using System.Collections;

public class ColonyManager : MonoBehaviour {
	
	private static string cellPrefabPath = "CellPrefab";
	private UnityEngine.Object cellPrefab;
	private SimulationManager manager;
	
	/* POOL in C# as Sorted List */
	private SortedList pool;
	
	// 2 gameobjects to divide
	private GameObject cellOne;
	private GameObject cellTwo;
	private Cell cellScriptOne;
	private Cell cellScriptTwo;
	
	/* 
	 * SIMULATION PARAMETERS 
	 */	
	
	public float divisionTime = 1.0f;
	public const float minDivisionTime = 0.1f;
	public const float maxDivisionTime = 10.0f;
	
	public float stemLatencyRatio = 2.0f;
	public const float minStemlatency = 0.1f;
	public const float maxStemlatency = 10.0f;
	
	public int progenitorDivisions = 7;
	public const int minProgenitorDivisions = 0;
	public const int maxProgenitorDivisions = 50;
	
	public int sizeSensor = 12;
	public int stemSensorType = 1;
	public const int minSizeSensor = 0;
	public const int maxSizeSensor = ColonyManager.MAXCELLS;
	
	public int progenitorSizeSensor = 100;
	private float progenitorSizeSensorTemp = 0.2f;
	public const int minProgenitorSizeSensor = 0;
	public const int maxProgenitorSizeSensor = ColonyManager.MAXCELLS;
	
	private float normalLatency;
	private float stemLatency;
	
	private float scaledDivisionTime;
	
	// MATERIAL for the DOPPIETTO (transparent)
	public Material doppiettoMaterial;
	
	public float divisionPreselectionTime = 6.0f; // how many hours before division I must preselect a cell ?
	private float _divisionPreSelectionTime;
	
	// NO RANDOM HERE: we use symmetric sequence of events
	public bool[] stem_symmetric_events;
	
	// MAX CELLS
	public const int MAXCELLS = 1000;
	public int cellCounter;   // because pool.Count is the list of TIME POINTS not the list of CELLS
	
	// TEMPERATURES
	public float timeDivisionTemp = 5.0f;
	public float sigSplitTemperature = 5.0f;
	
	// STATS CELL TYPES
	public int[] cellTypes;
	
	/* INIT 
	 * 
	 * Start is only called once in the lifetime of the behaviour. 
	 * The difference between Awake and Start is that Start is only called if the script instance is enabled. 
	 * This allows you to delay any initialization code, until it is really needed. Awake is always called before any Start functions. 
	 * This allows you to order initialization of scripts
	 *
	 */	
	void Start () 
	{
		pool = new SortedList();
		cellCounter = 0;
		stem_symmetric_events = new bool[10]; // SET ALL as FALSE (DEFAULT)				
		cellTypes = new int[3] {0,0,0};  // TO BE RESETTED AT EACH CALL of PoolTypesStats
		
		manager = GameObject.Find("SimulationManager").GetComponent<SimulationManager>();
		scaledDivisionTime = divisionTime * SimulationManager.ONEDAYCONSTAT;
		
		// set the division preselection time in Time.time
		float _scalePreselectionTime = 24.0f / divisionPreselectionTime;
		_divisionPreSelectionTime = SimulationManager.ONEDAYCONSTAT / _scalePreselectionTime;
			
		// Normal Latency is the scaledDivisionTime
		normalLatency = scaledDivisionTime;
		stemLatency = stemLatencyRatio * scaledDivisionTime;
		
		// LOAD CELL PREFABE
		cellPrefab = Resources.Load(cellPrefabPath);
		
		// Create the Firts ORIGIN cell
		if (!cellPrefab	) {
			Debug.LogError("No cell prefab!");
			return;
		}
		
		GameObject origin = InstantiateTheOrigin();
		
		// move orbit point to origin
		manager.MoveOrbitPoint(origin.transform.position);
	}	
	
	/****************************************************************************** 
	 * 
	 * 	CORE ACTION:
	 * - check if poll have cells and if pool have less than MAXCELLS
	 * - check next time points and START A COROUTINE for cell division
	 * - FixedUpdate should be used instead of Update when dealing with Rigidbody.
	 * 
	 ******************************************************************************/	
	void FixedUpdate ()
	{
		if (!manager.pause) {
			
			/* 
			 * DEBUG POOL STUFFS 
			 */
			
			//Debug.Log("Current pool size: " + PoolCount());
			//Debug.Log("Current pool keys: ");
			//IList mykeys = PoolKeys();			
			//for ( int i = 0; i < mykeys.Count; i++ )
			//	Debug.Log("Key: " + mykeys[i]);
			
			if (pool.Count > 0 && cellCounter < MAXCELLS) {
				
				// take the FIRST key, subtract the DIVISION_TIME
				float _timeForNextGroupOfCellsToDivide = (float) pool.GetKey(0);
				float _nextDivisionAnticipated = _timeForNextGroupOfCellsToDivide - _divisionPreSelectionTime;
				
				// START DIVIDE
				if (_nextDivisionAnticipated <= manager.time) {					
					//Debug.Log("NEXT DIVISION TIME: " + _timeForNextGroupOfCellsToDivide + ", NEXT DIV ANTICIPATED: " + _nextDivisionAnticipated + ", CURRENT TIME: " + manager.time);
					ArrayList theList = pool[_timeForNextGroupOfCellsToDivide] as ArrayList;
					foreach ( GameObject item in theList )
					{
						if (!item.GetComponent<Cell>().IsOnCamera()) {
							manager.PlayerLookAt(item);
						}
						item.GetComponent<Cell>().SelectCell();
					}
				}
				
				// DIVIDE
				if (_timeForNextGroupOfCellsToDivide <= manager.time) {				
					
					ArrayList theList = pool[_timeForNextGroupOfCellsToDivide] as ArrayList;
					ArrayList toRemove = new ArrayList();
					
					//Debug.Log("Time to divide group of cells at pool time: " + _timeForNextGroupOfCellsToDivide + " cells # " + theList.Count);
					//Debug.Log("TIME: " + manager.time + ". Time of divisions [D:H:M:S]: " + manager.Days() + ":" + manager.Hours() + ":" + manager.Minutes() + ":" + manager.Seconds());
					
					foreach ( GameObject item in theList )
					{
						Divide(item, _timeForNextGroupOfCellsToDivide);
						toRemove.Add(item);
					}
					
					foreach ( GameObject item in toRemove)
					{
						theList.Remove(item);
					}
					if (theList.Count == 0)
						pool.Remove(_timeForNextGroupOfCellsToDivide);
				}				
				UpdatePoolTypesStats(); // update the PoolStats!
			}
		}
	}
	
	private void Divide( GameObject cell, float time)
	{	
		switch (cell.GetComponent<Cell>().cellType) {
			case Cell.CellType.STEM:
				StemDivision(cell, time);
			break;

			case Cell.CellType.PROGENITOR:
				float current_division_counter = cell.GetComponent<Cell>().divisionLeft - 1;
				ProgenitorDivision(cell, time, current_division_counter);
			break;
			
			case Cell.CellType.DIFFERENTIATED:
				DifferentiatedKeep(cell, time);
			break;
			
			default:
				Debug.LogError("This cell does not have a type!");
			break;
		}
	}
	
	private void StemDivision( GameObject cell, float time)
	{
		// SIZE SENSOR
		// Rationale: se ho superato in size della sfera il size sensor, le staminali non dividono piu' [stemSensorType 1]
		if (sizeSensor != 0 && cellCounter >= sizeSensor && stemSensorType == 1) {
			//Debug.Log("STEM DIVISION over! SIZE SENSOR: " + sizeSensor + " current size: " + cellCounter);
			cell.GetComponent<Cell>().DeselectCell();
			// set next Divisin Time to current time limit
			cell.GetComponent<Cell>().nextDivisionTime = SimulationManager.ENDOFSIMULATION;
			// cell will be removed soon than I have to add manually to the poll
			AddToPoll(cell.GetComponent<Cell>().nextDivisionTime, cell);
			return;
		}
		
		// Debug.Log("STEM DIVISION NUMBER: " + cell.GetComponent<Cell>().stemEventsCounter);
		// this array store the symmetric events as TRUE
		if (sizeSensor != 0 && cellCounter >= sizeSensor && stemSensorType == 0) {
			// SIZE SENSOR BLOCKS ONLY SYMMETRIC DIVISIONS
			Debug.Log("Size Sensor for stem blocking symmetric divisions!");
			AsymmetricDivision(cell);
		} else {
			// NORMAL BEHAVIOUR
			if (stem_symmetric_events[cell.GetComponent<Cell>().stemEventsCounter] == true) {
				SymmetricDivision(cell);
			} else {
				AsymmetricDivision(cell);
			}
		}
		// ADD TO POOL the NEW CELLS
		AddToPoll(cellScriptOne.nextDivisionTime, cellOne);
		AddToPoll(cellScriptTwo.nextDivisionTime, cellTwo);
		KillCell(cell);
	}
	
	private void SymmetricDivision(GameObject cell)
	{
		// Debug.Log("Symmetric Division! " + stem_symmetric_events[cell.GetComponent<Cell>().stemEventsCounter] );
		CellDivision(cell);
		MakeStem(cellScriptOne, cell.GetComponent<Cell>());
		MakeStem(cellScriptTwo, cell.GetComponent<Cell>());
	}
	
	private void AsymmetricDivision(GameObject cell)
	{
		// Debug.Log("Asymmetric Division! " + stem_symmetric_events[cell.GetComponent<Cell>().stemEventsCounter] );
		CellDivision(cell);
		if (manager.GetMersenneRandDouble() > 0.5) {				
			MakeStem(cellScriptOne, cell.GetComponent<Cell>());
			MakeProgenitor(cellScriptTwo);
		} else {
			MakeProgenitor(cellScriptOne);
			MakeStem(cellScriptTwo, cell.GetComponent<Cell>());
		}
	}
	
	private void ProgenitorDivision( GameObject cell, float time, float current_division_counter)
	{
		// DECREMENT the DIVISION COUNTER if it is <= 0 is a DIFFERENTIATED
		// Debug.Log("DIVISIONS LEFT: " + current_division_counter);
		
		// PROGENITOR SIZE SENSOR
		if (progenitorSizeSensor != 0 && cellCounter >= progenitorSizeSensor) {
			float probDevelop = LogisticDecadence.LogDecadence(progenitorSizeSensorTemp, cellCounter, progenitorSizeSensor);
			if (manager.GetMersenneRandDouble() > probDevelop) {
				// Debug.Log("PROGENITOR SENSOR IN ACTION!");
				// Set a new division time and return
				cell.GetComponent<Cell>().DeselectCell();
				cell.GetComponent<Cell>().nextDivisionTime = manager.time + (normalLatency * manager.NormalizedRandom(timeDivisionTemp));
				AddToPoll(cell.GetComponent<Cell>().nextDivisionTime, cell);
				return;
			}
		}
		
		
		// HERE IF NO SIZE SENSOR
		CellDivision(cell);
		
		if (current_division_counter <= 0.0f) {
			MakeDifferentiated(cellScriptOne,  cell.GetComponent<Cell>());
			MakeDifferentiated(cellScriptTwo,  cell.GetComponent<Cell>());
		} else {
			MakeSonOfProgenitor(cellScriptOne, current_division_counter);
			MakeSonOfProgenitor(cellScriptTwo, current_division_counter);
		}
		
		// ADD TO POOL the NEW CELLS
		AddToPoll(cellScriptOne.nextDivisionTime, cellOne);
		AddToPoll(cellScriptTwo.nextDivisionTime, cellTwo);
		KillCell(cell);
	}
	
	private void MakeStem(Cell cs, Cell parent)
	{
		cs.cellType = Cell.CellType.STEM;
		cs.nextDivisionTime = manager.time + (stemLatency * manager.NormalizedRandom(timeDivisionTemp));
		cs.divisionLeft = Mathf.Infinity;
		cs.stemEventsCounter = parent.stemEventsCounter + 1;
		//Debug.Log("My Counter is: " + cs.stemEventsCounter + " my parent have " + parent.stemEventsCounter);
	}
	
	private void MakeProgenitor(Cell cs)
	{
		cs.cellType = Cell.CellType.PROGENITOR;
		cs.nextDivisionTime = manager.time + (normalLatency * manager.NormalizedRandom(timeDivisionTemp));
		// PROGRAM the PROGENITOR
		cs.divisionLeft = progenitorDivisions;
		// Debug.Log("PROGENITOR PROGRAMMED with DIVISIONS LEFT: " + cellScriptTwo.divisionLeft);
	}
	
	private void MakeSonOfProgenitor(Cell cs, float counter)
	{
		cs.cellType = Cell.CellType.PROGENITOR;
		cs.nextDivisionTime = manager.time + (normalLatency * manager.NormalizedRandom(timeDivisionTemp));
		cs.divisionLeft = counter;;
	}
	
	private void MakeDifferentiated(Cell cs, Cell parent)
	{
		cs.cellType = Cell.CellType.DIFFERENTIATED;
		/* 
		 * No Infinity because we use this in SortArray! 
		 * No current timeLimit or our array will fail if I stop pausing
		 * with 
		 * InvalidOperationException: List has changed.
		 * System.Collections.ArrayList+SimpleEnumerator.MoveNext ()
		 * ColonyManager.FixedUpdate () (at Assets/Scripts/models/ColonyManager.cs:179)
		 * 
		 */
		cs.nextDivisionTime = SimulationManager.ENDOFSIMULATION;
		cs.divisionLeft = 0;
	}
	
	private void DifferentiatedKeep( GameObject cell, float time)
	{
		Cell cellScript = cell.GetComponent<Cell>();
		cellScript.nextDivisionTime = manager.time + SimulationManager.ENDOFSIMULATION;
		// Add time and push in the Pool again
		AddToPoll(cellScript.nextDivisionTime, cell);
	}
	
	/*
	 * CORE METHOD CELL DIVISION
	 */	
	private void CellDivision( GameObject cell)
	{
		Vector3 pos = cell.transform.position;
		Vector3 newPosition = Random.onUnitSphere / 2.0f;
		Vector3 newCellPosOne = pos + newPosition;
		Vector3 newCellPosTwo = pos - newPosition;
		
		cellOne = InstantiateCell(pos, Quaternion.identity);
		cellTwo = InstantiateCell(pos, Quaternion.identity);
		
		GameObject doppietto = CreateDoppietto(pos, newCellPosOne);
		
		// STORE GENEALOGY INFORMATION
		// Debug.Log(cell.GetComponent<Cell>().genData.name);
		
		cell.GetComponent<Cell>().genData.timeDeath = manager.time;
		
		cellScriptOne = cellOne.GetComponent<Cell>();
		cellScriptTwo = cellTwo.GetComponent<Cell>();
		
		cellScriptOne.genealogy = new ArrayList(cell.GetComponent<Cell>().genealogy);
		cellScriptTwo.genealogy = new ArrayList(cell.GetComponent<Cell>().genealogy);
		
		cellScriptOne.genealogy.Add(cell.GetComponent<Cell>().genData);
		cellScriptTwo.genealogy.Add(cell.GetComponent<Cell>().genData);
		
		//Debug.Log(cellScriptTwo.genealogy.Count);
		
		//STAIN LEVEL RANDOMIZED
		// nc prende una quantità di Pkh che e' circa la meta' di quella della madre
		cellScriptOne.stainLevel = manager.NormalizedRandom(sigSplitTemperature) * cell.GetComponent<Cell>().stainLevel;
		cellScriptTwo.stainLevel = cell.GetComponent<Cell>().stainLevel - cellScriptOne.stainLevel;
		
		// BECAUSE I DESTROY ONE CELL AND I ADD 2 CELLS
		cellCounter++;
		
		// STORE DOPPIETTO INFO
		doppietto.GetComponent<Doppietto>().StoreDoppiettoInfo(cell.GetComponent<Cell>(), cellScriptOne, cellScriptTwo);
		
		// START MOVING TO TARGET with COROUTINES
		StartCoroutine(DivideCellCoroutine(cellOne, cellTwo, newCellPosOne, newCellPosTwo, doppietto));
		StartCoroutine(DuplicationCollider(doppietto));
	}
	
	private IEnumerator DivideCellCoroutine(GameObject cellOne, GameObject cellTwo, Vector3 endOne, Vector3 endTwo, GameObject doppietto)
	{
		yield return StartCoroutine( MoveCells(cellOne, cellTwo, endOne, endTwo) );
		yield return StartCoroutine( RemoveDoppietto(doppietto) );
		yield return StartCoroutine( CellAddRigidBodies(cellOne, cellTwo) );
		yield return StartCoroutine( MakeSticky(cellOne, cellTwo));
		//Debug.Log("cells are now sticky");
	}
	
	private IEnumerator MakeSticky(GameObject one, GameObject two)
	{
		// MAKE A JOINT BETWEEN the 2 CELLS
		if (one != null) {
			Joint joint = one.AddComponent<FixedJoint>();	
			if (two	 != null) {
    			joint.connectedBody = two.rigidbody;
				two.GetComponent<Cell>().isSticky = true;
			}			
			one.GetComponent<Cell>().isSticky = true;
		}
		
		yield return null;
	}
	
	private IEnumerator MoveCells(GameObject cellOne, GameObject cellTwo, Vector3 endOne, Vector3 endTwo)
	{
		float i = 0.0f;
		while (i < 1.0f) {
			i += manager.animationSpeed;
			if (cellOne	!= null)
				cellOne.transform.position = Vector3.Lerp(cellOne.transform.position, endOne, i);
			if (cellTwo	 != null)
				cellTwo.transform.position = Vector3.Lerp(cellTwo.transform.position, endTwo, i);
			yield return null;
		}
	}
	
	private IEnumerator RemoveDoppietto(GameObject doppietto)
	{
		Destroy(doppietto);
		yield return null;
	}
	
	private IEnumerator CellAddRigidBodies(GameObject cellOne, GameObject cellTwo)
	{
		// cells reach point
		// print("cell points reached");
		if (cellOne	!= null)
			cellOne.GetComponent<Cell>().EnablePhysics();		
		if (cellTwo	!= null)
			cellTwo.GetComponent<Cell>().EnablePhysics();
		yield return null;
	}
	
	private IEnumerator DuplicationCollider(GameObject doppietto)
	{
		float i_doppietto = 0.0f;	
		while (doppietto != null && doppietto.transform.localScale.y < 1.0f)
		{
			i_doppietto += manager.animationSpeed;
			doppietto.transform.localScale += new Vector3 (0.0f, i_doppietto, 0.0f);
			yield return null;
		}
	}
	
	private GameObject CreateDoppietto(Vector3 p, Vector3 cellOne_p)
	{
		GameObject doppiettoScaffold = GameObject.CreatePrimitive(PrimitiveType.Capsule);
		doppiettoScaffold.tag = "Doppietto";
		doppiettoScaffold.transform.position = p;
		doppiettoScaffold.transform.LookAt(cellOne_p);
		doppiettoScaffold.transform.Rotate(new Vector3(90.0f,0.0f,0.0f));
		doppiettoScaffold.transform.localScale = new Vector3 (1.0f, 0.5f, 1.0f);
		doppiettoScaffold.transform.renderer.material = doppiettoMaterial;
		
		Rigidbody doppiettoBody = doppiettoScaffold.AddComponent<Rigidbody>();
		doppiettoBody.useGravity = false;
		doppiettoBody.constraints = RigidbodyConstraints.FreezeAll;
		doppiettoBody.freezeRotation = true;
		doppiettoBody.mass = 0.2f;  // should be 2X a cell
		doppiettoBody.drag = 1.0f;
		doppiettoBody.angularDrag = 1.0f;
		
		// Doppietto should be have his component
		doppiettoScaffold.AddComponent<Doppietto>();
		
		return(doppiettoScaffold);
	}
	
	private void KillCell( GameObject cell)
	{
		Destroy(cell);
	}		
	
	private GameObject InstantiateCell(Vector3 p, Quaternion r)
	{
		GameObject cell = Instantiate(cellPrefab, p, r) as GameObject;
		
		// DISABLE PHYSICS ON CREATION
		cell.GetComponent<Cell>().DisablePhysics();
		return(cell);
	}
	
	/* CREATE THE FIRST CELL */
	private GameObject InstantiateTheOrigin()
	{
		cellCounter++;
		GameObject aCell = InstantiateCell(transform.position, Quaternion.identity);
		
		// set name, type and stain level
		aCell.name = "origin";
		Cell cellScript = aCell.GetComponent<Cell>();
		cellScript.cellType = Cell.CellType.STEM;
		
		// stain level
		cellScript.stainLevel = 1.0f;
		
		// set division time to next day ... is a STEM so infinite division potential
		cellScript.nextDivisionTime = manager.time + scaledDivisionTime;
		cellScript.divisionLeft = Mathf.Infinity;
		
		cellScript.EnablePhysics();
		cellScript.isSticky = true;
		
		// genealogy INIT empty
		cellScript.genealogy = new ArrayList();
		
		AddToPoll(cellScript.nextDivisionTime, aCell);
		
		UpdatePoolTypesStats();
		
		return aCell;
	}
	
	public void DebugPool()
	{
		// When you use foreach to enumerate hash table elements,
        // the elements are retrieved as KeyValuePair objects.
		foreach( DictionaryEntry de in pool )
        {
			Debug.Log("Time-->: " + de.Key);
			
			foreach ( GameObject i in de.Value as ArrayList)
			{
				Debug.Log("CELL ID:" + i.GetComponent<Cell>().cellID + " : Type: " + i.GetComponent<Cell>().cellType.ToString());
			}
		}
	}
	
	private void AddToPoll(float time, GameObject cell)
	{
		if (pool.ContainsKey(time)) {
			ArrayList list = pool[time] as ArrayList;
			list.Add(cell);
		} else {
			ArrayList list = new ArrayList();
			list.Add(cell);
			pool.Add(time, list);
		}
	}
	
	private int PoolCount()
	{
		return pool.Count;
	}
	
	public void DebugStemSymmetricArray()
	{
		for (int i = 0; i < stem_symmetric_events.Length; i++) {
			Debug.Log("element " + i + " --> " + stem_symmetric_events[i]);
		}
	}
	
	private IList PoolKeys()
	{
		return pool.GetKeyList();
	}
	
	// return an aray of 3 INT (STEM count, PROGENITOR count and DIFFERENTIATED count)
	public void UpdatePoolTypesStats()
	{	
		for (int i = 0; i < cellTypes.Length; i++) {
			cellTypes[i] = 0;	
		}
		
		foreach( DictionaryEntry de in pool )
		{
			foreach ( GameObject i in de.Value as ArrayList)
			{
				switch (i.GetComponent<Cell>().cellType) {
					case Cell.CellType.STEM:
						cellTypes[0]++;
					break;
					case Cell.CellType.PROGENITOR:
						cellTypes[1]++;
					break;
					case Cell.CellType.DIFFERENTIATED:
						cellTypes[2]++;
					break;
				}
			}
		}
	}
	
	// ON APPLICATION QUIT
	void OnApplicationQuit()
	{
		// GARBAGE COLLECT the POOL
		foreach( DictionaryEntry de in pool )
        {
			foreach ( GameObject i in de.Value as ArrayList)
			{
				KillCell(i);
			}
		}
		pool.Clear();
	}
	
	/* 
	 * EDITOR STUFFS
	 */
	
	// DRAW GIZMO CUBE
	void OnDrawGizmosSelected() 
	{
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireCube(transform.position, new Vector3(10, 10, 10));
    }

}
