/*
 * 
 * PLAYER
 * 
 * MOVEMENT INTERFACE:
 * 	
 * -
 * -
 * -
 * 
 * */

using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour 
{
	// OPTIONS MOVE
	public float fixedStep = 0.01f;
	public float keyMoveSpeed = 10.0f;
	
	public float mouseSensitivityX = 8.0f;
	public float mouseSensitivityY = 8.0f;
	
	public float orbitSpeedX = 250.0f;
	public float orbitSpeedY= 125.0f;
	public float orbitMinLimitY= -20.0f;
	public float orbitMaxLimitY= 80.0f;
	
	private float orbit_x = 0.0f;
	private float orbit_y = 0.0f;
	
	// SmoothLook: dampen the rotation
	public float smoothLookDamping = 6.0f;
	
	// SIMULATION MANAGER
	private SimulationManager manager;
	
	// Movements
	private Transform genealogy;
	public float distanceGenealogyCamera = 10.0f;
	private Vector3 positionAtColony;
	private Vector3 rotationAtColony;
	
	// GENERAL DISABLE for TUTORIAL
	public bool isActive = true;
	
	public enum CameraPosition
	{
		COLONY,
		GENEALOGY
	}	
	public CameraPosition currentCameraPosition;
	
	void Start () 
	{
		//  A common pattern is to assign a game object to a variable inside MonoBehaviour.Start. And use the variable in MonoBehaviour.Update.
		GameObject mobj = GameObject.Find("SimulationManager");	
		if (mobj != null) {
			manager = mobj.GetComponent<SimulationManager>();
		}
		
		GameObject gen = GameObject.Find("GenealogyManager");
		if (gen != null) {
			genealogy = gen.transform;
		}
		
		Vector3 angles = transform.eulerAngles;
		orbit_x = angles.x;
		orbit_y = angles.y;
		
		currentCameraPosition = Player.CameraPosition.COLONY;
		
		// Make the rigid body not change rotation
	   	if (rigidbody)
			rigidbody.freezeRotation = true;
		
		// store position and rotation
		positionAtColony = transform.position;
		rotationAtColony = transform.eulerAngles;
	}
	
	/* LateUpdate is called after all Update functions have been called. */
	void LateUpdate ()
	{	
		/*************************************************************************************************** 
		 * Animations should work also in pause (no TIme.deltaTime)
		 * so I use step
		 * http://answers.unity3d.com/questions/29881/how-to-pause-and-unpause-my-game-inputgetaxis-does.html
		 *****************************************************************************************************/
		float step = GetStep();
		
		if (isActive)
			MoveWithArrows(step);
		
		if (manager != null)
		{
			if (isActive && Input.GetButton("Fire2")) {
				MouseOrbit();
			}
			
			// NOT WORK IF SIMULATION DID'T START YET
			if (Input.GetButtonDown("Jump") && manager.simulationStarted) {
				manager.pause = !manager.pause;
			}
		}		
	}
	
	private float GetStep()
	{
		float step;
		if (manager != null && manager.pause) {
			step = fixedStep;
		} else {
			step = Time.deltaTime;
		}
		return step;
	}
	
	public void MoveToColony()
	{
		currentCameraPosition = Player.CameraPosition.COLONY;
		MoveCameraTo(positionAtColony);
		RotateCameraTo(rotationAtColony);
	}
	
	public void MoveToGenealogy()
	{
		// store current rotation and position
		positionAtColony = transform.position;
		rotationAtColony = transform.eulerAngles;
		currentCameraPosition = Player.CameraPosition.GENEALOGY;
		Vector3 pos = genealogy.position;
		pos -= Vector3.forward * distanceGenealogyCamera;
		MoveCameraTo(pos);
		RotateCameraTo(Vector3.zero);
	}
	
	private void MoveCameraTo(Vector3 p)
	{
		transform.position = p;
	}
	
	private void RotateCameraTo(Vector3 angle)
	{
		transform.eulerAngles = angle;
	}
	
	public void StartLookAt(Vector3 pos)
	{
		float step = GetStep();
		StartCoroutine(SmoothLookAt(pos - transform.position, step));
	}
	
	private IEnumerator SmoothLookAt(Vector3 p, float step)
	{
		Quaternion rotation = Quaternion.LookRotation(p);
		while (transform.rotation != rotation) {
			transform.rotation = Quaternion.Slerp(transform.rotation, rotation, step * smoothLookDamping);
			yield return null;
		}
	}
	
	void MoveWithArrows(float step)
	{		
		float key_x, key_y, key_height;
		
		if (manager != null && manager.pause) {
			key_x = Input.GetAxisRaw("Horizontal") / keyMoveSpeed;
			key_y = Input.GetAxisRaw("Vertical") / keyMoveSpeed;
			key_height = Input.GetAxisRaw("Height") / keyMoveSpeed;
		} else {
			key_x = Input.GetAxis("Horizontal") * step * keyMoveSpeed;
			key_y = Input.GetAxis("Vertical") * step * keyMoveSpeed;
			key_height = Input.GetAxis("Height") * step * keyMoveSpeed;
		}
		
		if (key_x != 0)
			Strafe(key_x);
		
		if (key_y != 0)
			MoveForwards(key_y);
		
		if (key_height != 0)
			ChangeHeight(key_height);
	}
	
	void MouseOrbit()
	{
		orbit_x += Input.GetAxis("Mouse X") * orbitSpeedX * 0.02f;
		orbit_y -= Input.GetAxis("Mouse Y") * orbitSpeedY * 0.02f;
		orbit_y = ClampAngle(orbit_y, orbitMinLimitY, orbitMaxLimitY);
		Quaternion rotation = Quaternion.Euler(orbit_y,orbit_x,0.0f);
		float dist = Vector3.Distance(transform.position, manager.orbitPoint.position);
		Vector3 position = rotation * new Vector3(0.0f,0.0f,-dist) + manager.orbitPoint.position;
		transform.rotation = rotation;
		transform.position = position;
	}

	/********************** 
	 * CAMERA ACTIONS
	 ***********************/
	
	void MoveForwards(float aVal)
    {
        Vector3 fwd = transform.forward;
        fwd.y = 0;
        fwd.Normalize();
        transform.position += aVal * fwd;
    }
	
	void Strafe(float aVal)
	{
		transform.position += aVal * transform.right;
	}

    void ChangeHeight(float aVal)
    {
        transform.position += aVal * Vector3.up;
    }

	public static void WrapAngle(ref float angle)
	{
	    if (angle < -360F)
	        angle += 360F;
	    if (angle > 360F)
	        angle -= 360F;
	}
	
	public static float ClampAngle (float angle, float min, float max) 
	{
		WrapAngle(ref angle);
		return Mathf.Clamp (angle, min, max);
	}
	
	/* Editor STUFFS: GIZMO */
	void OnDrawGizmos() 
	{
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, 0.2f);
    }
	
}