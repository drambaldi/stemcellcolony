using UnityEngine;
using System.Collections;


// Helper to make capsule doppietto sticky

public class Doppietto : MonoBehaviour {

	public bool isSticky = true;
	
	public DoppiettoData data;
	public struct DoppiettoData
	{
		public Cell origin;
		public Cell cellA;
		public Cell cellB;
	}
	
	// CollisionEnter
	void OnCollisionEnter(Collision c) {
		
		if (isSticky) {
			MakeStickyCollision(c);
		}
    }
	
	private void MakeStickyCollision(Collision c)
	{			
		// OPTIMIZATION check if alredy there is a joint
		if ((gameObject.GetComponent<FixedJoint>() != null) && (gameObject.GetComponent<FixedJoint>().connectedBody == c.rigidbody)) {
			//Debug.Log("HUSTON we already have a JOINT connected to that cell!");
			return;
		} else if ((c.gameObject.GetComponent<FixedJoint>() != null) && (c.gameObject.GetComponent<FixedJoint>().connectedBody == gameObject.rigidbody)) {
			//Debug.Log("HUSTON we already have a JOINT connected from that cell!");
			return;
		} else {
			Joint joint = gameObject.AddComponent<FixedJoint>();
			joint.connectedBody = c.rigidbody;
		}
	}
	
	public void StoreDoppiettoInfo(Cell origin, Cell cellA, Cell cellB)
	{
		data.origin = origin;
		data.cellA = cellA;
		data.cellB = cellB;
	}
}
