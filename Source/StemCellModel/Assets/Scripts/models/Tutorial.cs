using UnityEngine;
using System.Collections;
using GUISounds;

public class Tutorial : MonoBehaviour {
	
	// SOUNDS
	public AudioClip clickSound;
	
	private static string _welcome = "Welcome to Stem Cells coloniy simulator.\nThis tutorial will teach you how to use the interface!\n";	
	private static string _red_sphere =	"The red sphere under the tutorial window is the stem cell that originate the colony.";
	private static string _inspector =	"The bottom-right window is The Inspector. You can have some information about the current selected cell.";
	private static string _parameters =	"In the Simulation panel you can change the simulation parameters, when you press OK the simulation start. For now press CONTINUE on this Tutorial window";
	private static string _simulation = "THE SIMULATION IS RUNNING and you have full control on interface... press CLOSE to go back to Main Menu. PRESS THE HELP MENU while in simlation to get more HELP";
		
	// THE GUI OBJECT and THE PLAYER
	private StemCellModelGUI theGUIObject;
	private Player player;
	
	// WINDOW
	public bool drawTutorialWindow;
	
	private Rect _small;
	private Rect _big;
	private Rect tutorialBoxRect;
	
	
	// CUSTOM SKIN
	public GUISkin tutorialSkin;
	
	public enum TutorialStep
	{
		INTRO,
		THECELL,
		PARAMETERS,
		SIMULATION
	}	
	public TutorialStep currentStep;
	
	// SIMULATION MANAGER
	private SimulationManager manager;
	
	// Use this for initialization
	void Start () 
	{
		
		GameObject mobj = GameObject.Find("SimulationManager");	
		if (mobj != null) {
			manager = mobj.GetComponent<SimulationManager>();
		}
		
		drawTutorialWindow = true;
		
		// DISABLE PLAYER
		player  = GameObject.Find("Player").GetComponent<Player>();
		player.isActive = false;
		theGUIObject = GameObject.Find("GUIObject").GetComponent<StemCellModelGUI>();
		theGUIObject.isActive = false;
		theGUIObject.drawSimulationPanel = false;
		_small =  new Rect( Screen.width / 2 - 300, Screen.height / 2 - 300, 600, 200); // CENTER
		_big = new Rect( Screen.width / 2 - 300, Screen.height / 2 - 200, 600, 400); // CENTER
		tutorialBoxRect = _big;
		currentStep = TutorialStep.INTRO;
	}
	
	// OnGUI
	void OnGUI () 
	{
		GUI.skin = tutorialSkin;
		if (drawTutorialWindow)
			tutorialBoxRect = GUILayout.Window(1, tutorialBoxRect, DoTutorialWindow, "Tutorial Window", GUI.skin.GetStyle("window"));
	}
	
	private void DoTutorialWindow(int windowID)
	{
		GUILayout.BeginVertical("box");
		
		if (currentStep == TutorialStep.INTRO) {
			SelectStemOrigin();
			GUILayout.Label(_welcome);
			GUILayout.FlexibleSpace();
			GUILayout.Label(_red_sphere);
			
			GUILayout.BeginHorizontal();
			GUILayout.FlexibleSpace();
			if (GUILayout.Button("CONTINUE",GUILayout.Height(60.0f), GUILayout.Width(100.0f)).Play(clickSound)) 
			{
				currentStep = TutorialStep.THECELL;
				tutorialBoxRect = _small;
				theGUIObject.drawInspectorPanel = true;
			}
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();
			
		} else if (currentStep == TutorialStep.THECELL) {
			GUILayout.Label(_inspector);
			GUILayout.FlexibleSpace();
			
			GUILayout.BeginHorizontal();
			GUILayout.FlexibleSpace();
			if (GUILayout.Button("CONTINUE",GUILayout.Height(60.0f), GUILayout.Width(100.0f)).Play(clickSound)) 
			{
				currentStep = TutorialStep.PARAMETERS;
				tutorialBoxRect = _small;
				theGUIObject.drawInspectorPanel = false;
				theGUIObject.drawSimulationPanel = true;
			}
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();
		} else if (currentStep == TutorialStep.PARAMETERS) {
			
			GUILayout.Label(_parameters);
			GUILayout.FlexibleSpace();
			
			GUILayout.BeginHorizontal();
			GUILayout.FlexibleSpace();
			if (GUILayout.Button("CONTINUE",GUILayout.Height(60.0f), GUILayout.Width(100.0f)).Play(clickSound)) 
			{
				currentStep = TutorialStep.SIMULATION;
				tutorialBoxRect = _small;
				theGUIObject.drawSimulationPanel = false;
				manager.pause = false;
				theGUIObject.isActive = true;
				player.isActive = true;
			}
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();
			
		} else if (currentStep == TutorialStep.SIMULATION) {
			
			GUILayout.Label(_simulation);
			GUILayout.FlexibleSpace();
			
			GUILayout.BeginHorizontal();
			GUILayout.FlexibleSpace();
			if (GUILayout.Button("CLOSE",GUILayout.Height(60.0f), GUILayout.Width(100.0f)).Play(clickSound)) 
			{
				Application.LoadLevel("MainMenu");
			}
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();	
		}
		
		GUILayout.EndVertical();
		GUI.DragWindow();
	}
	
	void SelectStemOrigin()
	{
		GameObject origin = GameObject.Find("origin");
		manager.Select(origin);
	}
}
