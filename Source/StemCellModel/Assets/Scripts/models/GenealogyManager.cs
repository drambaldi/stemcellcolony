using UnityEngine;
using System.Collections;

public class GenealogyManager : MonoBehaviour {
	
	private static string cellPrefabPath = "CellPrefab";
	private UnityEngine.Object cellPrefab;
	
	// Use this for initialization
	void Start () {
		cellPrefab = Resources.Load(cellPrefabPath);
		if (!cellPrefab	) {
			Debug.LogError("No cell prefab!");
			return;
		}
		//GameObject aCell = InstantiateCell(transform.position, Quaternion.identity);
	}
	
	public void InitGenealogyView( GameObject cell)
	{
		Vector3 moveUp = new Vector3(0.0f, 1.0f, 0.0f);
		Vector3 currentPosition = transform.position;
		
		// INSTANTIATE SELF
		InstantiateCellCopy(currentPosition, Quaternion.identity, cell);
		
		// REVERSE THE GENEALOGY
		ArrayList _temp = cell.GetComponent<Cell>().genealogy;
		_temp.Reverse();
		
		// INSTANTITATE THE GENEALOGY
		foreach ( Cell.GenealogyData obj in _temp )
		{
			currentPosition += moveUp;
			//Debug.Log("CELL: name: " + obj.name + " - type: " + obj.type + " - div left: " + obj.divisionLeft + " - born: " + obj.timeBorn + " - death: " + obj.timeDeath);
			InstatiateFromGenData(currentPosition, Quaternion.identity, obj);
		}
		_temp.Clear();
	}
	
	public void ClearGenealogyView()
	{
		GameObject[] cells = GameObject.FindGameObjectsWithTag("GenealogyCell");
		for (int i=0;i<cells.Length;i++)
		{
			Destroy(cells[i]);
		}
		//Debug.Log ("Genalogy View CLEAR");
	}
	
	// DRAW GIZMO CUBE
	void OnDrawGizmosSelected() 
	{
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireCube(transform.position, new Vector3(10, 10, 10));
    }
	
	// Instantiate a Cell
	private GameObject InstatiateFromGenData(Vector3 p, Quaternion r, Cell.GenealogyData data)
	{
		GameObject cell = Instantiate(cellPrefab, p, r) as GameObject;
		cell.GetComponent<Cell>().CloneFromGenData(data);
		cell.tag = "GenealogyCell";
		
		// DISABLE RIGID BODY
		cell.GetComponent<Cell>().EnableCollider();
		cell.GetComponent<Cell>().DisableRigidBody();
		return(cell);
	}
	
	private GameObject InstantiateCellCopy(Vector3 p, Quaternion r, GameObject original)
	{
		GameObject cell = Instantiate(cellPrefab, p, r) as GameObject;
		cell.GetComponent<Cell>().CloneCell(original);
		cell.tag = "GenealogyCell";
		
		// DISABLE RIGID BODY
		cell.GetComponent<Cell>().EnableCollider();
		cell.GetComponent<Cell>().DisableRigidBody();
		return(cell);
	}
}
