/*******************************************************************
 * CELL CLASS
 * 
 * VARS:	
 *  divisionLeft FLOAT
 *  nextDivisionTime FLOAT
 * 	cellType ENUM
 * 
 * Tag: Cell
 * 
 * Materials order: 
 *    STEM, PROGENITOR, DIFFERENTIATED, DEFAULT, 
 *    STEM SELECTED, PROGENITOR SELECTED, DIFFERENTIATED SELECTED
 * 
 * 	AWAKE --> START
 * 
 * Start is only called once in the lifetime of the behaviour. 
 * The difference between Awake and Start is that Start is only called if the script instance is enabled. 
 * This allows you to delay any initialization code, until it is really needed. 
 * Awake is always called before any Start functions. 
 * This allows you to order initialization of scripts
 * 
 ********************************************************************/

using UnityEngine;
using System.Collections;

public class Cell : MonoBehaviour 
{	
	public enum CellType
	{
		STEM,
		PROGENITOR,
		DIFFERENTIATED
	}	
	public CellType cellType;
	public Material[] materials;
	
	// GENEALOGY
	public GenealogyData genData;
	public struct GenealogyData
	{
		public string name;
		public CellType type;
		public float divisionLeft;
		public float timeBorn;
		public float timeDeath;
		public float stainLevel;
	}
	public ArrayList genealogy;
	
	// CELLID
	private string _cellID;	
	public string cellID 
	{
		get
		{
			return _cellID;
		}
	}
	
	// PROPERTIES
	public bool isSticky = true;
	private bool isSelected;
	
	// DIVISION VARS
	public float divisionLeft = 0.0f;
	public float nextDivisionTime = Mathf.Infinity;
	
	// COLOR
	public float stainLevel = 0.0f;
	private Color stainColor;
	
	// CELL SPECIFIC SYMMETRICEVENTS COUNTER
	private int _stemEventsCounter = 0;
	private int stemEventsArrayLength = 0;
	public int stemEventsCounter
	{
		get
		{
			return _stemEventsCounter;
		}
		
		set
		{
			_stemEventsCounter = value;
			// increase the stem divisions cause this is a STEM symmetric division, if is too big, return to begin
			// Debug.Log("ArrayLength in CEll: " + stemEventsArrayLength);
			if (_stemEventsCounter	>= stemEventsArrayLength)
				_stemEventsCounter = 0;
		}
			
	}
	
	private SimulationManager manager;
	private ColonyManager cmanager;
	
	// array of joints to remove them is dead joints
	private FixedJoint[] joints;

	public void CloneCell(GameObject cell)
	{
		Cell cellScript = cell.GetComponent<Cell>();
		name = cellScript.name;
		cellType = cellScript.cellType;
		divisionLeft = cellScript.divisionLeft;
		genData = cellScript.genData;
		stainLevel = cellScript.stainLevel;
	}
	
	public void CloneFromGenData(GenealogyData data)
	{
		name = data.name;
		cellType = data.type;
		divisionLeft = data.divisionLeft;
		stainLevel = data.stainLevel;
		genealogy = null;
	}
	
	void Awake()
	{
		GameObject mobj = GameObject.Find("SimulationManager");
		if (mobj != null)
			manager = mobj.GetComponent<SimulationManager>();
		
		GameObject cmobj = GameObject.Find("ColonyManager");
		if (cmobj != null)
			cmanager = cmobj.GetComponent<ColonyManager>();
		
		if (cmanager != null)
			stemEventsArrayLength = cmanager.stem_symmetric_events.Length;
		
		isSelected = false;
		//Debug.Log("Array length set: " + stemEventsArrayLength);
	}
	
	void Start () 
	{			
		if (materials.Length < 7)
			Debug.LogError("Incomplete Cell Material Array");
		
		_cellID = RandomString.MakeRandomString(20);
		
		// compute stain color level
		stainColor = WaveToRgb.StainToRgb(stainLevel);
		
		// new cells should behave as old cells already in play
		if (manager.showStain) {
			ShowStain();
		} else {
			switch (cellType) 
			{			
				case CellType.STEM:
					transform.renderer.material = materials[0];
					if (!manager.showStem) MakeTransparent();
				break;
				
				case CellType.PROGENITOR:
					transform.renderer.material = materials[1];
					if (!manager.showProgenitor) MakeTransparent();
				break;
				
				case CellType.DIFFERENTIATED:
					transform.renderer.material = materials[2];
					if (!manager.showDifferentiated) MakeTransparent();
				break;
				
				default:
					transform.renderer.material = materials[3];
				break;
			}
		}
		// prepare genealogy data
		genData.name = cellID;
		genData.type = cellType;
		genData.divisionLeft = divisionLeft;
		genData.timeBorn = manager.time;
		genData.stainLevel = stainLevel;
	}
		
	// CollisionEnter
	void OnCollisionEnter(Collision c) 
	{
		if (isSticky) {
			MakeStickyCollision(c);
		}
    }
	
	// CollisionExit
	void OnCollisionExit(Collision c)
	{	
		// print("collision exit for cell: " + GetComponent<Cell>().cellID);
		// optimization: removed dead fixed joints
		c.rigidbody.velocity = Vector3.zero;
		rigidbody.velocity = Vector3.zero;
		joints = GetComponents<FixedJoint>();
		foreach (FixedJoint joint in joints) {
			if (joint.connectedBody == null)
				Destroy(joint);
		}
	}
	
	private void MakeStickyCollision(Collision c)
	{
		if (gameObject.tag == "Cell") {
			
			// OPTIMIZATION check if alredy there is a joint
			if ((gameObject.GetComponent<FixedJoint>() != null) && (gameObject.GetComponent<FixedJoint>().connectedBody == c.rigidbody)) {
				//Debug.Log("HUSTON we already have a JOINT connected to that cell!");
				return;
			} else if ((c.gameObject.GetComponent<FixedJoint>() != null) && (c.gameObject.GetComponent<FixedJoint>().connectedBody == gameObject.rigidbody)) {
				//Debug.Log("HUSTON we already have a JOINT connected from that cell!");
				return;
			} else {
    			Joint joint = gameObject.AddComponent<FixedJoint>();
    			joint.connectedBody = c.rigidbody;
			}
		}
	}
	
	public bool IsOnCamera()
	{
		return renderer.isVisible;
	}
	
	public bool IsStem()
	{
		return (cellType == Cell.CellType.STEM ? true : false);
	}
	
	public bool IsProgenitor()
	{
		return (cellType == Cell.CellType.PROGENITOR ? true : false);
	}
	
	public bool IsDifferentiated()
	{
		return (cellType == Cell.CellType.DIFFERENTIATED ? true : false);
	}
	
	// SELECTION
	public void SelectCell()
	{
		isSelected = true;
	}
	
	public void DeselectCell()
	{
		isSelected = false;
	}
	
	public void MakeTransparent()
	{
		transform.renderer.material = materials[7];
	}
	
	public void MakeSolid()
	{
		// when I make it solid is is Selected I must move to selectedCellMaterial
		switch (cellType) 
		{			
			case CellType.STEM:
				transform.renderer.material = isSelected ? materials[4] : materials[0];
			break;
			
			case CellType.PROGENITOR:
				transform.renderer.material = isSelected ? materials[5] : materials[1];
			break;
			
			case CellType.DIFFERENTIATED:				
				transform.renderer.material = isSelected ? materials[6] : materials[2];
			break;
		}
	}
	
	// Enable/Disable All Physics
	public void EnablePhysics()
	{
		EnableCollider();
		EnableRigidBody();
	}
	
	public void DisablePhysics()
	{
		DisableCollider();
		DisableRigidBody();
		if (isSticky) isSticky = false;
	}
	
	// Enable/Disable RigidBody
	public void EnableRigidBody()
	{
		gameObject.rigidbody.isKinematic = false;
	}
	
	public void DisableRigidBody()
	{
		gameObject.rigidbody.isKinematic = true;
	}
	
	// Enable/Disable Collider
	public void EnableCollider()
	{
		transform.collider.enabled = true;
	}
	
	public void DisableCollider()
	{
		transform.collider.enabled = false;
	}
	
	// EVENTS
    public void OnDisable()
    {
		if (manager != null)
    		manager.Deselect (gameObject);
    }
	
	public void OnDestroy() 
	{
		if (manager != null)
			manager.Deselect (gameObject);
    }
	
	// STAIN COLOR FUNCTIONS
	public void ShowStain()
	{
		// AREA 1: R -> 1.0, G -> 0.0, B -> 0.0
		transform.renderer.material = materials[8];
		transform.renderer.material.color = stainColor;
	}
}
