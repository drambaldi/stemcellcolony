using UnityEngine;
using System.Collections;

// CUSTOM
using GUISounds;

public class StemCellModelGUI : MonoBehaviour {

	// SOUNDS
	public AudioClip clickSound;
	
	// CUSTOM SKIN
	public GUISkin stemCellSkin;
	
	// WINDOWS BOOLEAN
	public bool drawControlPanel = true;
	public bool drawInspectorPanel = false;
	public bool drawDebugPanel = false;
	public bool drawSimulationPanel = true;
	public bool drawHelpPanel = false;
	public bool drawMain = true;
	public bool drawGenealogy = false;
	public bool drawGenealogyInspector = false;
	
	/* CONTROL PANEL */
	private int controlPanelWidth = 700;
	private int controlPanelHeight = 100;
	private System.String playLabel;
	private int buttonWidth = 100;
	
	/* COMON WINDOW MARGIN */
	private int windowMargin = 10;
	
	// MANAGERS
	private SimulationManager manager;
	private ColonyManager cmanager;
	
	// Windows
	private Rect debugBoxRect;
	private Rect inspectorBoxRect;
	private Rect simulationBoxRect;
	private Rect helpBoxRect;
	private Rect genealogyInspectorBoxRect;
	
	// SPECTRUM TEXTURE
	public GUITexture spectrumTexture;
	
	// STEM SIZE SENSOR TOOLBAR
	public string[] stemsensorToolbarStrings = new string[] {"Stop Symmetric Events", "Stop Divisions"};
	
	// SEED string
	public string stringSeed;
	
	// GENERAL DISABLE FOR TUTORIAL
	public bool isActive = true;
	
	void Awake () 
	{
		manager  = GameObject.Find("SimulationManager").GetComponent<SimulationManager>();
		cmanager = GameObject.Find("ColonyManager").GetComponent<ColonyManager>();
	}
	
	void Start ()
	{
		if (Debug.isDebugBuild)
			debugBoxRect = new Rect( windowMargin,(windowMargin * 2) + controlPanelHeight,400,300); // TOP LEFT
		
		inspectorBoxRect = new Rect( Screen.width - 300 - windowMargin,Screen.height - 400 - windowMargin, 300, 400); // BOTTOM RIGHT
		simulationBoxRect = new Rect( Screen.width / 2 - 300, Screen.height / 2 - 200, 600, 400); // CENTER
		helpBoxRect = new Rect( windowMargin, Screen.height - 600 - windowMargin, 350, 600); // BOTTOM LEFT
		genealogyInspectorBoxRect = new Rect( Screen.width - 300 - windowMargin,Screen.height - 400 - windowMargin, 300, 400); // BOTTOM RIGHT
		stringSeed = manager.seed.ToString();
	}
	
	void Update ()
	{
		if (manager.showStain) {
			spectrumTexture.enabled = true;
		} else {
			spectrumTexture.enabled = false;
		}
	}
	
	void OnGUI () 
	{
		
		GUI.skin = stemCellSkin;
		
		Event e = Event.current;
		
		playLabel = manager.pause ? "PLAY": "PAUSE";
		
		if (drawMain) {
			
			if (drawControlPanel) DoControlPanel();
			
			if (Debug.isDebugBuild && drawDebugPanel)
				debugBoxRect = GUILayout.Window(1, debugBoxRect, DoDebugWindow, "Debug", GUI.skin.GetStyle("window"));
	
			if (drawInspectorPanel)
				inspectorBoxRect = GUILayout.Window(2, inspectorBoxRect, DoInspectorWindow, "Inspector", GUI.skin.GetStyle("window"));
	
			if (drawSimulationPanel)
				simulationBoxRect = GUILayout.Window(3, simulationBoxRect, DoSimulationWindow, "Simulation", GUI.skin.GetStyle("window"));
			
			if (drawHelpPanel)
				helpBoxRect = GUILayout.Window(4, helpBoxRect, DoHelpWindow, "Help", GUI.skin.GetStyle("window"));
			
		} else if (drawGenealogy) {
			
			DoGenealogyPanel();
			
			if (drawGenealogyInspector)
				genealogyInspectorBoxRect = GUILayout.Window(5, genealogyInspectorBoxRect, DoGenealogyInspectorWindow,"Genealogy Inspector",GUI.skin.GetStyle("window"));
		
		}
		
		// check for OnMouseDown events that are not used by GUI
		if (e.button == 0 && e.isMouse)
			manager.SelectWithMouse();
	}
	
	private void DoGenealogyPanel()
	{
		// same position of controlPanel
		GUILayout.BeginArea (new Rect (windowMargin,windowMargin,controlPanelWidth,controlPanelHeight));
		
		// Begin the singular Horizontal Group
		GUILayout.BeginHorizontal();
		
		if (GUILayout.Button("BACK", GUILayout.Width(buttonWidth), GUILayout.Height(controlPanelHeight/2 - 5)).Play(clickSound)) 
		{
			if (isActive)
				manager.PlayerMoveToColonyView();
		}
		
		GUILayout.BeginVertical("box");			
			GUILayout.BeginHorizontal();
			GUILayout.FlexibleSpace();
			GUILayout.Label("GENEALOGY VIEW KEY SHORTCUTS");
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();
		
			GUILayout.BeginHorizontal();
			GUILayout.FlexibleSpace();
			GUILayout.Label("W-A-S-D: movement X,Z axis");
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();
		
		
			GUILayout.BeginHorizontal();
			GUILayout.FlexibleSpace();
			GUILayout.Label("Q-E: movement Y axes");
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();		
		GUILayout.EndVertical();
		
		GUILayout.EndHorizontal();
		
		GUILayout.EndArea();
	}
	
	private void DoControlPanel()
	{
		// Wrap everything in the designated GUI Area
		GUILayout.BeginArea (new Rect (windowMargin,windowMargin,controlPanelWidth,controlPanelHeight));
		
		// Begin the singular Horizontal Group
		GUILayout.BeginHorizontal();
		
		GUILayout.BeginVertical();
		
		if (GUILayout.Button(playLabel, GUILayout.Width(buttonWidth), GUILayout.Height(controlPanelHeight/2 - 5)).Play(clickSound)) 
		{
			if (isActive)
				manager.pause = !manager.pause;
		}
		
		if (GUILayout.Button("RESET", GUILayout.Width(buttonWidth), GUILayout.Height(controlPanelHeight/2 - 5)).Play(clickSound)) 
		{
			if (isActive)
				Application.LoadLevel(Application.loadedLevel);	
		}
		
		GUILayout.EndVertical();
		
		GUILayout.BeginVertical();
		
		if (GUILayout.Button("CONFIG", GUILayout.Width(buttonWidth), GUILayout.Height(controlPanelHeight/2 - 5)).Play(clickSound)) 
		{
			if (isActive)
				drawSimulationPanel = !drawSimulationPanel;
		}
		
		if (GUILayout.Button("HELP", GUILayout.Width(buttonWidth), GUILayout.Height(controlPanelHeight/2 - 5)).Play(clickSound)) {
			if (isActive)
				drawHelpPanel = !drawHelpPanel;
		}
		
		GUILayout.EndVertical();
		
		GUILayout.BeginVertical();
		
		if (GUILayout.Button("INFO", GUILayout.Width(buttonWidth), GUILayout.Height(controlPanelHeight/2 - 5)).Play(clickSound)) 
		{
			if (isActive)
				drawInspectorPanel = !drawInspectorPanel;
		}
		
		if (Debug.isDebugBuild) {
			if (GUILayout.Button("DEBUG", GUILayout.Width(buttonWidth), GUILayout.Height(controlPanelHeight/2 - 5)).Play(clickSound)) 
			{
				if (isActive)
					drawDebugPanel = !drawDebugPanel;
			}
		}
		GUILayout.EndVertical();		
		
		// Arrange two more Controls vertically beside the Button
		GUILayout.BeginVertical(); // ANINOMOUS CONTAINER
		
		GUILayout.BeginVertical("box", GUILayout.Width(350.0f), GUILayout.Height(controlPanelHeight / 2.0f - 20));
		
		GUILayout.Label("Time [D:H:M:S] : " + manager.Days() + " : " + manager.Hours() + " : " 
		                + manager.Minutes() + " : " + manager.Seconds() + " ~ AppTime: " + manager.time);		
		GUILayout.BeginHorizontal();
		GUILayout.Label("Speed: " + manager.speed, GUILayout.Width(100.0f));
		if (isActive)
		{
			manager.speed = (int) GUILayout.HorizontalSlider(manager.speed, 0.0f, manager.speedValues.Length - 1);
		} else {
			GUILayout.HorizontalSlider(manager.speed, 0.0f, manager.speedValues.Length - 1);
		}
		GUILayout.EndHorizontal();		
		
		
		// End the Groups and Area
		GUILayout.EndVertical();
			GUILayout.BeginHorizontal("box", GUILayout.Width(350.0f), GUILayout.Height(controlPanelHeight / 2.0f - 15));
		
			if (isActive)
			{
				manager.showStem = GUILayout.Toggle(manager.showStem, "Stem");
			} else {
				GUILayout.Toggle(manager.showStem, "Stem");
			}
		
			if (isActive)
			{
				manager.showProgenitor = GUILayout.Toggle(manager.showProgenitor, "Progenitor");
			} else {
				GUILayout.Toggle(manager.showProgenitor, "Progenitor");
			}
		
			if (isActive)
			{
				manager.showDifferentiated = GUILayout.Toggle(manager.showDifferentiated, "Differentiated");
			} else {
				GUILayout.Toggle(manager.showDifferentiated, "Differentiated");
			}
			
			if (isActive)
			{
				manager.showStain = GUILayout.Toggle(manager.showStain, "Stain");
			} else {
				GUILayout.Toggle(manager.showStain, "Stain");
			}
		
			GUILayout.EndHorizontal();		
		GUILayout.EndVertical();
		
		GUILayout.FlexibleSpace();
		
		GUILayout.EndHorizontal();
		GUILayout.EndArea();
	}

	// DEBUG GUI
	void DoDebugWindow(int windowID)
	{
		GUILayout.FlexibleSpace();
		GUILayout.BeginVertical("box");
		
		if (GUILayout.Button("Debug Pool").Play(clickSound))
			cmanager.DebugPool();
		
		if (GUILayout.Button("Debug Symetric Events Array").Play(clickSound))
			cmanager.DebugStemSymmetricArray();
		
		if (GUILayout.Button("Debug Random String").Play(clickSound))
			Debug.Log( RandomString.MakeRandomString(10) );
		
		if (GUILayout.Button("Debug Pool Stats").Play(clickSound)) {
			cmanager.UpdatePoolTypesStats();
			Debug.Log("STEM: " + cmanager.cellTypes[0]);
			Debug.Log("PROGENITOR: " + cmanager.cellTypes[1]);
			Debug.Log("DIFFERENTIATED: " + cmanager.cellTypes[2]);			
		}
		
		GUILayout.EndVertical();
		GUILayout.FlexibleSpace();
		
		GUILayout.BeginVertical("box");
		
		if (GUILayout.Button("TEST TRANSPARENT").Play(clickSound)) {
			GameObject[] cells = GameObject.FindGameObjectsWithTag("Cell");
			for (int i=0;i<cells.Length;i++)
			{
				cells[i].GetComponent<Cell>().MakeTransparent();
			}
		}
		
		if (GUILayout.Button("TEST SOLID").Play(clickSound)) {
			GameObject[] cells = GameObject.FindGameObjectsWithTag("Cell");
			for (int i=0;i<cells.Length;i++)
			{
				cells[i].GetComponent<Cell>().MakeSolid();
			}
		}
		
		if (GUILayout.Button("SHOW STEM").Play(clickSound)) {
			GameObject[] cells = GameObject.FindGameObjectsWithTag("Cell");
			for (int i=0;i<cells.Length;i++)
			{	
				if (cells[i].GetComponent<Cell>().IsStem()) {
					cells[i].GetComponent<Cell>().MakeSolid();
				} else {
					cells[i].GetComponent<Cell>().MakeTransparent();
				}
			}
		}
		
		if (GUILayout.Button("SHOW PROGENITORS").Play(clickSound)) {
			GameObject[] cells = GameObject.FindGameObjectsWithTag("Cell");
			for (int i=0;i<cells.Length;i++)
			{	
				if (cells[i].GetComponent<Cell>().IsProgenitor()) {
					cells[i].GetComponent<Cell>().MakeSolid();
				} else {
					cells[i].GetComponent<Cell>().MakeTransparent();
				}				
			}
		}
		
		if (GUILayout.Button("SHOW DIFFERENTIATED").Play(clickSound)) {
			GameObject[] cells = GameObject.FindGameObjectsWithTag("Cell");
			for (int i=0;i<cells.Length;i++)
			{	
				if (cells[i].GetComponent<Cell>().IsDifferentiated()) {
					cells[i].GetComponent<Cell>().MakeSolid();
				} else {
					cells[i].GetComponent<Cell>().MakeTransparent();
				}				
			}
		}
		
		if (GUILayout.Button("SHOW STAIN LEVEL").Play(clickSound)) {
			GameObject[] cells = GameObject.FindGameObjectsWithTag("Cell");
			for (int i=0;i<cells.Length;i++)
			{
				cells[i].GetComponent<Cell>().ShowStain();
			}
		}
		
		GUILayout.EndVertical();
		
		GUILayout.FlexibleSpace();
		GUI.DragWindow();
	}
	
	void DoHelpWindow(int windowID)
	{
		GUILayout.FlexibleSpace();
		GUILayout.BeginVertical("box");			
			GUILayout.BeginHorizontal();
			GUILayout.FlexibleSpace();
			GUILayout.Label("KEY SHORTCUTS");
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();
			GUILayout.Label("W-A-S-D: movement X,Z axis");
			GUILayout.Label("Q-E: movement Y axes");
			GUILayout.Label("SPACE: pause/resume simulation");
			GUILayout.Label("LEFT MOUSE BUTTON: select an object");
			GUILayout.Label("RIGHT MOUSE BUTTON + MOUSE MOVE: orbit view");
		GUILayout.EndVertical();
		GUILayout.FlexibleSpace();
		GUILayout.BeginVertical("box");
			GUILayout.BeginHorizontal();
			GUILayout.FlexibleSpace();
			GUILayout.Label("BUTTONS:");
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();
			GUILayout.Label("PLAY/PAUSE: simulation time regulation");
			GUILayout.Label("CONFIG: Open configuration window");
			GUILayout.Label("INFO: Open Information window");
			GUILayout.Label("RESET: restart simulation");
			GUILayout.Label("HELP: This Window");
			GUILayout.Label("DEBUG: Debug Window");
			GUILayout.Label("Speed: simulation speed control");
			GUILayout.Label("VIEW CONTROL: Activate/Deactivate view types");
		GUILayout.EndVertical();
		GUILayout.FlexibleSpace();
		GUILayout.BeginVertical("box");
			GUILayout.BeginHorizontal();
			GUILayout.FlexibleSpace();
			GUILayout.Label("WINDOWS:");
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();
			GUILayout.Label("Simulation: configure the simulation");
			GUILayout.Label("Inspector: show infos");
			GUILayout.Label("Help: this window");
			GUILayout.Label("Debug: debug window");
		GUILayout.EndVertical();
		GUILayout.FlexibleSpace();
		GUI.DragWindow();
	}
	
	void DoInspectorWindow(int windowID)
	{
		GUILayout.BeginVertical("box");	
		GUILayout.Label("Colony Size:" + cmanager.cellCounter);
		GUILayout.Label("STEM: " + cmanager.cellTypes[0]);
		GUILayout.Label("PROGENITOR: " + cmanager.cellTypes[1]);
		GUILayout.Label("DIFFERENTIATED: " + cmanager.cellTypes[2]);	
		GUILayout.EndVertical();
		
		if (manager.ActiveSelection) {
			GUILayout.BeginVertical("box");	
			GUILayout.Label("Current Selected Cell:");	
				
			GUILayout.Label("\tTYPE:" + manager.ActiveSelection.GetComponent<Cell>().cellType);
			if (manager.ActiveSelection.GetComponent<Cell>().cellType == Cell.CellType.STEM)
				GUILayout.Label("\tSTEM DIVISIONS: " + manager.ActiveSelection.GetComponent<Cell>().stemEventsCounter);
			
			GUILayout.Label("\tID:" + manager.ActiveSelection.GetComponent<Cell>().cellID);
			GUILayout.Label("\tDivision Left:" + manager.ActiveSelection.GetComponent<Cell>().divisionLeft);
			GUILayout.Label("\tNext Division Time:" + (manager.ActiveSelection.GetComponent<Cell>().nextDivisionTime));
			GUILayout.Label("\tStain Level:" + (manager.ActiveSelection.GetComponent<Cell>().stainLevel));
			GUILayout.Label("\tGenealogy Count:" + (manager.ActiveSelection.GetComponent<Cell>().genealogy.Count));
			
			if (GUILayout.Button("SHOW GENEALOGY").Play(clickSound)) 
			{
				if (isActive)
					manager.PlayerMoveToGenealogyView();
			}
			
			GUILayout.EndVertical();
		}
		GUI.DragWindow();
	}	
	
	void DoGenealogyInspectorWindow(int windowID)
	{
		GUILayout.BeginVertical("box");	
		GUILayout.Label("Current Selected Cell:");	
			
		GUILayout.Label("\tTYPE:" + manager.ActiveSelection.GetComponent<Cell>().cellType);
		GUILayout.Label("\tDivision Left:" + manager.ActiveSelection.GetComponent<Cell>().divisionLeft);
		GUILayout.Label("\tStain Level:" + (manager.ActiveSelection.GetComponent<Cell>().stainLevel));
		
		GUILayout.EndVertical();
		GUI.DragWindow();
	}
	
	void DoSimulationWindow(int windowID)
	{
		// works like a state machine
		if (manager.simulationStarted) {
			GUI.enabled = false;
		}
		
		float colwidth = simulationBoxRect.width / 2.0f;
		
		GUILayout.FlexibleSpace(); // SPACER
		
		GUILayout.BeginVertical("box");
		
			GUILayout.BeginHorizontal();
			GUILayout.FlexibleSpace();
			GUILayout.Label("STEM CELLS");
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();
		
			GUILayout.BeginHorizontal();
			GUILayout.Label("Stem Cell Division Ratio. Every " + (cmanager.divisionTime * cmanager.stemLatencyRatio) + " days");		
			if (isActive)
			{
				cmanager.stemLatencyRatio = GUILayout.HorizontalSlider(cmanager.stemLatencyRatio, ColonyManager.minStemlatency, ColonyManager.maxStemlatency,  GUILayout.Width(colwidth));
			} else {
				GUILayout.HorizontalSlider(cmanager.stemLatencyRatio, ColonyManager.minStemlatency, ColonyManager.maxStemlatency,  GUILayout.Width(colwidth));
			}
	        GUILayout.EndHorizontal();
		
			GUILayout.BeginHorizontal();		
			GUILayout.Label("Program the symmetric/asymmetric\nsequence of events for STEM cells", GUILayout.Width(colwidth));
		
			GUILayout.BeginVertical();		
			GUILayout.BeginHorizontal();
			for (int i = 0; i < cmanager.stem_symmetric_events.Length / 2; i++) {
				int ilabel = i + 1;
				if (isActive)
				{
					cmanager.stem_symmetric_events[i] = GUILayout.Toggle(cmanager.stem_symmetric_events[i], ilabel.ToString());
				} else {
					GUILayout.Toggle(cmanager.stem_symmetric_events[i], ilabel.ToString());
				}	
			}
			GUILayout.EndHorizontal();	
		
			GUILayout.BeginHorizontal();
			for (int i = 5; i < cmanager.stem_symmetric_events.Length; i++) {
				int ilabel = i + 1;
				if (isActive)
				{
					cmanager.stem_symmetric_events[i] = GUILayout.Toggle(cmanager.stem_symmetric_events[i], ilabel.ToString());
				} else {
					GUILayout.Toggle(cmanager.stem_symmetric_events[i], ilabel.ToString());
				}
			}
			GUILayout.EndHorizontal();		
			GUILayout.EndVertical();
	        GUILayout.EndHorizontal();
	
			GUILayout.BeginHorizontal();		
			GUILayout.Label("STEM SIZE SENSOR: " + cmanager.sizeSensor + "\n" + "Set to 0 to disable");
			if (isActive)
			{
				cmanager.sizeSensor = (int) GUILayout.HorizontalSlider(cmanager.sizeSensor, ColonyManager.minSizeSensor, ColonyManager.maxSizeSensor,  GUILayout.Width(colwidth));
			} else {
				GUILayout.HorizontalSlider(cmanager.sizeSensor, ColonyManager.minSizeSensor, ColonyManager.maxSizeSensor,  GUILayout.Width(colwidth));
			}
			GUILayout.EndHorizontal();
		
			GUILayout.BeginHorizontal();
			GUILayout.Label("STEM SIZE SENSOR TYPE:");
			cmanager.stemSensorType = GUILayout.Toolbar(cmanager.stemSensorType, stemsensorToolbarStrings);
			GUILayout.EndHorizontal();
		GUILayout.EndVertical(); // END FIRST BOX
		
		GUILayout.FlexibleSpace(); 
		
		GUILayout.BeginVertical("box");		
			GUILayout.BeginHorizontal();
			GUILayout.FlexibleSpace();
			GUILayout.Label("PROGENITORS");
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();
		
			GUILayout.BeginHorizontal();
			GUILayout.Label("Progenitor Division Ratio. Every: " + cmanager.divisionTime + " days");
			if (isActive)
			{
				cmanager.divisionTime = GUILayout.HorizontalSlider(cmanager.divisionTime, ColonyManager.minDivisionTime, ColonyManager.maxDivisionTime,  GUILayout.Width(colwidth));
			} else {
				GUILayout.HorizontalSlider(cmanager.divisionTime, ColonyManager.minDivisionTime, ColonyManager.maxDivisionTime,  GUILayout.Width(colwidth));
			}
	        GUILayout.EndHorizontal();
	
			GUILayout.BeginHorizontal();
			GUILayout.Label("A Progenitor is programmed to make " + cmanager.progenitorDivisions + " divisions");
			if  (isActive)
			{
				cmanager.progenitorDivisions = (int) GUILayout.HorizontalSlider(cmanager.progenitorDivisions, ColonyManager.minProgenitorDivisions, ColonyManager.maxProgenitorDivisions,  GUILayout.Width(colwidth));
			} else {
				GUILayout.HorizontalSlider(cmanager.progenitorDivisions, ColonyManager.minProgenitorDivisions, ColonyManager.maxProgenitorDivisions,  GUILayout.Width(colwidth));
			}
			GUILayout.EndHorizontal();
			
			GUILayout.BeginHorizontal();		
			GUILayout.Label("PROGENITOR SIZE SENSOR: " + cmanager.progenitorSizeSensor + "\n" + "Over this size the probability of division decades.\nSet to 0 to disable");
			if (isActive)
			{
				cmanager.progenitorSizeSensor = (int) GUILayout.HorizontalSlider(cmanager.progenitorSizeSensor, ColonyManager.minProgenitorSizeSensor, ColonyManager.maxProgenitorSizeSensor,  GUILayout.Width(colwidth));
			} else {
				GUILayout.HorizontalSlider(cmanager.progenitorSizeSensor, ColonyManager.minProgenitorSizeSensor, ColonyManager.maxProgenitorSizeSensor,  GUILayout.Width(colwidth));
			}
			GUILayout.EndHorizontal();		
		GUILayout.EndVertical(); // END SECOND BOX
		
		GUILayout.FlexibleSpace(); 
		
		GUILayout.BeginVertical("box");
			GUILayout.BeginHorizontal();
			GUILayout.FlexibleSpace();
			GUILayout.Label("TIME");
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();
			GUILayout.BeginHorizontal();		
			GUILayout.Label("TimeLimit in days: " + manager.timeLimit);
			if (isActive)
			{
				manager.timeLimit = (int) GUILayout.HorizontalSlider(manager.timeLimit, 1, SimulationManager.ENDOFSIMULATION,  GUILayout.Width(colwidth));
			} else {
				GUILayout.HorizontalSlider(manager.timeLimit, 1, SimulationManager.ENDOFSIMULATION,  GUILayout.Width(colwidth));
			}
			GUILayout.EndHorizontal();
		
			char chr = Event.current.character;
			if (chr < '0' || chr > '9') {
     		   Event.current.character = '\0';
    		}
		
			GUILayout.BeginHorizontal();		
			GUILayout.Label("RandomSeed:");
			if (isActive)
			{
				stringSeed = GUILayout.TextField(stringSeed);
			} else {
				GUILayout.TextField(stringSeed);
			}
			
			checked {
				try {
					manager.seed = int.Parse(stringSeed);
				} catch (System.Exception) {
					manager.seed = 12345;
				}
			}
			GUILayout.EndHorizontal();
		
		GUILayout.EndVertical(); // END THIRD BOX
		
		GUILayout.FlexibleSpace(); // SPACER
		
		GUILayout.BeginVertical("box");
		
			GUILayout.BeginHorizontal();
			GUILayout.FlexibleSpace();
			GUILayout.Label("SELECT PARAMETERS AND PRESS OK TO START SIMULATION");
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();
			
			GUILayout.BeginHorizontal();
			GUILayout.FlexibleSpace();
			if (GUILayout.Button("OK", GUILayout.Width(100), GUILayout.Height(30)).Play(clickSound)) 
			{
				if (isActive) {
					drawSimulationPanel = false;
					manager.pause = false;
				}
			}
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();
			
		GUILayout.EndVertical(); // END 4 BOX
		
		GUILayout.FlexibleSpace(); // SPACER
		
		if (manager.simulationStarted) {
			GUI.enabled = true;
		}
		
        GUI.DragWindow();
	}
	
	//EDITOR LOCATION GIZMO
	void OnDrawGizmos() 
	{
        Gizmos.color = new Color(1, 0, 0, 0.5F);
        Gizmos.DrawWireCube(transform.position, new Vector3(1, 1, 1));
    }
}
