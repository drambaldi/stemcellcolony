using UnityEngine;
using System.Collections;
using GUISounds;

public class MainMenu : MonoBehaviour {
	
	// SOUNDS
	public AudioClip clickSound;
	
	// CUSTOM SKIN
	public GUISkin mainMenuSkin;
	
	private int mainMenuWidth = 400;
	private int mainMenuMargin = 100;
	private int mainMenuHeight;
	private int mainMenuXAlign;
	private int mainMenuYAlign;
	private int mainMenuPadding = 25;
	private int mainMenuButtonWidth;
	private int mainMenuButtonSpacer;
	private int mainMenuButtonHeight = 100;
	
	void Awake()
    {       
        //Make this script persistent(Not destroy when loading a new level)
        DontDestroyOnLoad(this);
		
        Time.timeScale = 1.0f; //In case some game does not UN-pause..
		
    }
	
	void OnGUI()
	{
		GUI.skin = mainMenuSkin;
		
		 //Detect if we're in the main menu scene
        if (Application.loadedLevelName == "MainMenu")
        {
			mainMenuHeight = (mainMenuPadding * 6) + (mainMenuButtonHeight * 3);
			mainMenuXAlign = (Screen.width / 2) - (mainMenuWidth / 2);
			mainMenuYAlign = mainMenuMargin;
			mainMenuButtonWidth = mainMenuWidth - (mainMenuPadding * 2);
			
            MainMenuGUI();
        }
        else
        {
            //Game scene
            InGameGUI();
        }	
	}
	
	void MainMenuGUI()
	{
		// FIXED LAYOUT
		
		// Make a group on the center of the screen
		GUI.BeginGroup (new Rect ( mainMenuXAlign, mainMenuYAlign, mainMenuWidth, mainMenuHeight ));
		// All rectangles are now adjusted to the group. (0,0) is the topleft corner of the group.
	
		GUI.Box (new Rect (0, 0, mainMenuWidth, mainMenuHeight), "StemCell Colony");
		
		if (GUI.Button(new Rect (mainMenuPadding,mainMenuPadding * 2, mainMenuButtonWidth, mainMenuButtonHeight), "New Simulation").Play(clickSound))
		{
			Application.LoadLevel("Simulation");
		}
		
		if (GUI.Button(new Rect (mainMenuPadding, mainMenuButtonHeight + (mainMenuPadding * 3), mainMenuButtonWidth, mainMenuButtonHeight), "Tutorial").Play(clickSound))
		{
			Application.LoadLevel("Tutorial");
		}
		
		if (Application.platform == RuntimePlatform.WindowsPlayer || 
			Application.platform == RuntimePlatform.OSXPlayer || 
			Application.platform == RuntimePlatform.OSXEditor ||
			Application.platform == RuntimePlatform.WindowsEditor) {
			if (GUI.Button(new Rect (mainMenuPadding, (mainMenuButtonHeight*2) + (mainMenuPadding * 4), mainMenuButtonWidth, mainMenuButtonHeight), "Quit").Play(clickSound))
			{
				Application.Quit();
			}
		}
		
		// End the group we started above. This is very important to remember!
		GUI.EndGroup ();
	}
	
	void InGameGUI()
	{
		// AUTOMATIC LAYOUT
		
		//Top-right
        GUILayout.BeginArea(new Rect(0, 0, Screen.width, 100));
        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        if (GUILayout.Button("Back to menu"))
        {
            Destroy(gameObject); //Otherwise we'd have two of these..
            Application.LoadLevel(0);
        }
        GUILayout.EndHorizontal();
        GUILayout.EndArea();
	}
}
