using UnityEngine;
using System.Collections;

namespace GUISounds
{
	public static class SoundExtension
    {
		static AudioSource audio;
		
		public static bool Play ( this bool value, AudioClip clip )
        {
            if ( !clip || !value ) return value;

            EnsureAudioExists( );
            audio.PlayOneShot( clip );
            return value;
        }
		
		public static void DestroyAudio ( )
        {
            if ( audio )
            {
                GameObject.Destroy( audio.gameObject );
            }
        }

        public static void CreateAudio ( )
        {
            GameObject audioObject = new GameObject( "GUISounds Audio Extension" );
            audio = audioObject.AddComponent<AudioSource>( );
        }

        private static void EnsureAudioExists ( )
        {
            if ( !audio )
            {
                CreateAudio( );
            }
        }
	}
}