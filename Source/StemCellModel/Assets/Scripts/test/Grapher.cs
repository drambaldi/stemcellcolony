using UnityEngine;

public class Grapher : MonoBehaviour {
	
	public int resolution = 2000;
	private int currentResolution;
	private ParticleSystem.Particle[] points;
	private float x;
	
	void Start () {
		Time.timeScale = 1.0f;	
		CreatePoints();
		x  = -15.0f;
	}

	private void CreatePoints () {
		if(resolution < 2){
			resolution = 2;
		}
		currentResolution = resolution;
		points = new ParticleSystem.Particle[resolution];
		float increment = 0.015f;
		for(int i = 0; i < resolution; i++){
			x = x + increment;
			points[i].position = new Vector3(x, 0f, 0f);
			points[i].color = new Color(x, 0f, 0f);
			points[i].size = 0.1f;
		}
	}

	void Update () {
		if(currentResolution != resolution){
			CreatePoints();
		}
		for(int i = 0; i < resolution; i++){
			Vector3 p = points[i].position;
			// move all points to the left			
			p.y = Sine(p.x);
			points[i].position = p;
			Color c = points[i].color;
			c.g = p.y;
			points[i].color = c;		
			
		}			
		particleSystem.SetParticles(points, points.Length);
	}

	private static float Sine (float x){
		return 0.5f + 0.5f * Mathf.Sin(2 * Mathf.PI * x + Time.timeSinceLevelLoad);
	}
}