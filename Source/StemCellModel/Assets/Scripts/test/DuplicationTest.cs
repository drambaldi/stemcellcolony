using UnityEngine;
using System.Collections;

public class DuplicationTest : MonoBehaviour {
	
	public GameObject cell;
	public Material transparentMaterial;
	
	public float divisionTime = 3.0f;
	private float rate = 0.0f;
	private float i = 0.0f;
	private float i_doppietto = 0.0f;
	private float rate_doppietto = 0.0f;
	
	private float startDivision = 0.0f;
	private float endDivision = 0.0f;
	
	// Use this for initialization
	void Awake()
	{
        print("AWAKE");
		
		// was freezed ...
		Time.timeScale = 1.0f;
	}
	
	void Start () 
	{
		print("START");
	}
	
	IEnumerator DivideCellCoroutine(GameObject cellOne, GameObject cellTwo, Vector3 endOne, Vector3 endTwo, GameObject doppietto)
	{
		yield return StartCoroutine( MoveCells(cellOne, cellTwo, endOne, endTwo) );
		yield return StartCoroutine( RemoveDoppietto(doppietto) );
		yield return StartCoroutine( CellAddRigidBodies(cellOne, cellTwo) );
		endDivision = (float) Time.time;
		Debug.Log("End of division coroutine: " + endDivision);
		Debug.Log("Total division time: " + (endDivision - startDivision));
	}
	
	IEnumerator MoveCells(GameObject cellOne, GameObject cellTwo, Vector3 endOne, Vector3 endTwo)
	{
		i = 0.0f;
		rate = 1.0f / divisionTime;
		while (i < 1.0f) {
			i += Time.deltaTime * rate;
			cellOne.transform.position = Vector3.Lerp(cellOne.transform.position, endOne, i);
			cellTwo.transform.position = Vector3.Lerp(cellTwo.transform.position, endTwo, i);
			yield return null;
		}
	}
	
	IEnumerator RemoveDoppietto(GameObject doppietto)
	{
		Destroy(doppietto);
		yield return null;
	}
	
	IEnumerator CellAddRigidBodies(GameObject cellOne, GameObject cellTwo)
	{
		// cells reach point
		print("cell points reached");
	
		Rigidbody r1 = cellOne.AddComponent<Rigidbody>();
		r1.useGravity = false;
		cellOne.transform.collider.enabled = true;

		Rigidbody r2 = cellTwo.AddComponent<Rigidbody>();
		r2.useGravity = false;
		cellTwo.transform.collider.enabled = true;
		
		yield return null;
	}
	
	IEnumerator DuplicationCollider(GameObject doppietto)
	{
		i_doppietto = 0.0f;
		rate_doppietto = 1.0f / divisionTime;		
		while (doppietto != null && doppietto.transform.localScale.y <= 1.0f)
		{
			i_doppietto += Time.deltaTime * rate_doppietto;
			doppietto.transform.localScale += new Vector3 (0.0f, i_doppietto, 0.0f);
			yield return null;
		}
	}
	
	void DivideCells()
	{
		startDivision = Time.time;
		
		Vector3 pos = cell.transform.position;
		Vector3 newPosition = Random.insideUnitSphere * 0.5f;
		Vector3 newCellPosOne = pos + newPosition;
		Vector3 newCellPosTwo = pos - newPosition;
		Debug.Log("Cell is at: " + pos + ": newPosition = " + newPosition);
		Debug.Log("Cell posOne: " + newCellPosOne + ". Cell posTwo = " + newCellPosTwo);
		Destroy(cell);
				
		GameObject cellOne = GameObject.CreatePrimitive(PrimitiveType.Sphere);			
		GameObject cellTwo = GameObject.CreatePrimitive(PrimitiveType.Sphere);
		
		// DISABLE COLLIDERS
		cellOne.transform.collider.enabled = false;
		cellTwo.transform.collider.enabled = false;
		
		// TEST DOPPIETTO SCAFFOLD
		GameObject doppiettoScaffold = GameObject.CreatePrimitive(PrimitiveType.Capsule);
		doppiettoScaffold.transform.position = pos;
		doppiettoScaffold.transform.LookAt(newCellPosOne);
		doppiettoScaffold.transform.Rotate(new Vector3(90.0f,0.0f,0.0f));
		doppiettoScaffold.transform.localScale = new Vector3 (1.0f, 0.5f, 1.0f);
		doppiettoScaffold.transform.renderer.material = transparentMaterial;
		
		Rigidbody doppiettoBody = doppiettoScaffold.AddComponent<Rigidbody>();
		doppiettoBody.useGravity = false;
		doppiettoBody.constraints = RigidbodyConstraints.FreezeAll;
		doppiettoBody.freezeRotation = true;
		doppiettoBody.mass = 0.2f;  // should be 2X a cell
		doppiettoBody.drag = 1.0f;
		doppiettoBody.angularDrag = 1.0f;	
		
		cellOne.transform.position = pos;
		cellTwo.transform.position = pos;
		
		Debug.Log("Starting division time: " + startDivision);
		
		// START MOVING TO TARGET COROUTINES
		StartCoroutine(DivideCellCoroutine(cellOne, cellTwo, newCellPosOne, newCellPosTwo, doppiettoScaffold));
		StartCoroutine(DuplicationCollider(doppiettoScaffold));
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		
		if(Input.GetButtonDown("Jump") && cell != null)
		{
			print("DIVIDE");
			
			DivideCells();
		}
	}
}
