using UnityEngine;
using System.Collections;

public class StainTest : MonoBehaviour {
	
	public GameObject sphere;
	
	public float hSliderValue = 0;
	
	public const float MinVisibleWaveLength = 350.0f;
	public const float MaxVisibleWaveLength = 650.0f;
	public const float Gamma = 0.80f;
	public const int IntesityMax = 255;
		
	void OnGUI () 
	{
		GUILayout.BeginArea (new Rect (10,10,200,100));
		hSliderValue = GUILayout.HorizontalSlider(hSliderValue, 0.0f, 1.0f);
        GUILayout.Label("Color Test: " + hSliderValue);
		GUILayout.EndArea();
	}
	
	// Use this for initialization
	void Start () 
	{
		Time.timeScale = 1.0f;	
	}
	
	// Update is called once per frame
	void Update () 
	{
		Color c = WaveToRgb.StainToRgb(hSliderValue);
		sphere.renderer.material.color = c;
	}
	
}
