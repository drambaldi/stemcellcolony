using UnityEngine;
using System.Collections;

public class CellDuplicationTest : MonoBehaviour {
	
	public GameObject cell;
	public Material transparentMaterial;
	
	private static string cellPrefabPath = "CellPrefab";
	private UnityEngine.Object cellPrefab;
	
	private float startDivision = 0.0f;
	private float endDivision = 0.0f;
	
	public float divisionTime = 3.0f;
	private float rate = 0.0f;
	private float i = 0.0f;
	private float i_doppietto = 0.0f;
	private float rate_doppietto = 0.0f;
	
	// Use this for initialization
	void Awake()
	{
        print("AWAKE");
		
		// was freezed ...
		Time.timeScale = 1.0f;
	}
	
	void Start()
	{
		// LOAD CELL PREFABE
		cellPrefab = Resources.Load(cellPrefabPath);
	}

	void FixedUpdate () {
		
		if(Input.GetButtonDown("Jump") && cell != null)
		{
			print("DIVIDE");			
			DivideCells();
		}
	}
	
	void DivideCells()
	{
		startDivision = Time.time;
		Vector3 pos = cell.transform.position;
		Vector3 newPosition = Random.onUnitSphere / 2.0f;
		print("VECTOR: " + newPosition);
		Vector3 newCellPosOne = pos + newPosition;
		Vector3 newCellPosTwo = pos - newPosition;
		Debug.Log("Cell is at: " + pos + ": newPosition = " + newPosition);
		Debug.Log("Cell posOne: " + newCellPosOne + ". Cell posTwo = " + newCellPosTwo);
		Destroy(cell);
		
		GameObject cellOne = InstantiateCell(pos, Quaternion.identity);
		GameObject cellTwo = InstantiateCell(pos, Quaternion.identity);
		
		cellOne.GetComponent<Cell>().DisablePhysics();
		cellTwo.GetComponent<Cell>().DisablePhysics();
		
		GameObject doppietto = CreateDoppietto(pos, newCellPosOne);
		
		Debug.Log("Starting division time: " + startDivision);
		
		// START MOVING TO TARGET COROUTINES
		StartCoroutine(DivideCellCoroutine(cellOne, cellTwo, newCellPosOne, newCellPosTwo, doppietto));
		StartCoroutine(DuplicationCollider(doppietto));
	}
	
	GameObject InstantiateCell(Vector3 p, Quaternion r)
	{
		GameObject cell = Instantiate(cellPrefab, p, r) as GameObject;
		return(cell);
	}
	
	GameObject CreateDoppietto(Vector3 p, Vector3 cellOne_p)
	{
		GameObject doppiettoScaffold = GameObject.CreatePrimitive(PrimitiveType.Capsule);
		doppiettoScaffold.transform.position = p;
		doppiettoScaffold.transform.LookAt(cellOne_p);
		doppiettoScaffold.transform.Rotate(new Vector3(90.0f,0.0f,0.0f));
		doppiettoScaffold.transform.localScale = new Vector3 (1.0f, 0.5f, 1.0f);
		doppiettoScaffold.transform.renderer.material = transparentMaterial;
		
		Rigidbody doppiettoBody = doppiettoScaffold.AddComponent<Rigidbody>();
		doppiettoBody.useGravity = false;
		doppiettoBody.constraints = RigidbodyConstraints.FreezeAll;
		doppiettoBody.freezeRotation = true;
		doppiettoBody.mass = 0.2f;  // should be 2X a cell
		doppiettoBody.drag = 1.0f;
		doppiettoBody.angularDrag = 1.0f;
		
		return(doppiettoScaffold);
	}
	
	IEnumerator DivideCellCoroutine(GameObject cellOne, GameObject cellTwo, Vector3 endOne, Vector3 endTwo, GameObject doppietto)
	{
		yield return StartCoroutine( MoveCells(cellOne, cellTwo, endOne, endTwo) );
		yield return StartCoroutine( RemoveDoppietto(doppietto) );
		yield return StartCoroutine( CellAddRigidBodies(cellOne, cellTwo) );
		endDivision = (float) Time.time;
		Debug.Log("End of division coroutine: " + endDivision);
		Debug.Log("Total division time: " + (endDivision - startDivision));
		yield return StartCoroutine( MakeSticky(cellOne, cellTwo));
		Debug.Log("cells are now sticky");
	}
	
	IEnumerator MakeSticky(GameObject one, GameObject two)
	{
		one.GetComponent<Cell>().isSticky = true;
		two.GetComponent<Cell>().isSticky = true;
		yield return null;
	}
	
	IEnumerator MoveCells(GameObject cellOne, GameObject cellTwo, Vector3 endOne, Vector3 endTwo)
	{
		i = 0.0f;
		rate = 1.0f / divisionTime;
		while (i < 1.0f) {
			i += Time.deltaTime * rate;
			cellOne.transform.position = Vector3.Lerp(cellOne.transform.position, endOne, i);
			cellTwo.transform.position = Vector3.Lerp(cellTwo.transform.position, endTwo, i);
			yield return null;
		}
	}
	
	IEnumerator RemoveDoppietto(GameObject doppietto)
	{
		Destroy(doppietto);
		yield return null;
	}
	
	IEnumerator CellAddRigidBodies(GameObject cellOne, GameObject cellTwo)
	{
		// cells reach point
		print("cell points reached");
		cellOne.GetComponent<Cell>().EnablePhysics();
		cellTwo.GetComponent<Cell>().EnablePhysics();	
		yield return null;
	}
	
	IEnumerator DuplicationCollider(GameObject doppietto)
	{
		i_doppietto = 0.0f;
		rate_doppietto = 1.0f / divisionTime;		
		while (doppietto != null && doppietto.transform.localScale.y <= 1.0f)
		{
			i_doppietto += Time.deltaTime * rate_doppietto;
			doppietto.transform.localScale += new Vector3 (0.0f, i_doppietto, 0.0f);
			yield return null;
		}
	}
}
